$(document).ready(function () {
    $(".mini-cart-content-close").click(function () {
        //$(this).parents(".mini-cart-content").addClass('hidden');
        $(".mini-cart-wrap .mini-cart-content").slideUp().addClass('out');
    });
    $(".mini-cart-wrap .mini-cart,.mini-cart-counter").click(function () {
        //$(".mini-cart-wrap .mini-cart-content").removeClass('hidden');
        if (!$(".mini-cart-wrap .mini-cart-content").hasClass('out')) {
            $(".mini-cart-wrap .mini-cart-content").slideUp().addClass('out');
        } else {
            $(".mini-cart-wrap .mini-cart-content").slideDown().removeClass('out');
        }
        return false;
    });


    //bootstrap dropdown hover menu 
    $('ul.nav li.dropdown').hover(function () {
        //$(this).find('.dropdown-menu').stop(true, true).fadeIn(500);
        //open
        $(this).addClass("open");
    }, function () {
        //$(this).find('.dropdown-menu').stop(true, true).fadeOut(500);
        $(this).removeClass("open");
    });

});
