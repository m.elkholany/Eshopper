<?php
/**
 * Copyright © 2016 Neoxero. All rights reserved.
 * See COPYING.txt for license details.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::THEME,
    'frontend/NX/bareq_en',
    __DIR__
);
