<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Controller\Adminhtml\Layer;

use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Save Layer action.
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Save extends \Neoxero\Revslider\Controller\Adminhtml\Layer
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        $resultRedirect = $this->_resultRedirectFactory->create();

        if ($data = $this->getRequest()->getPostValue()) {
            $model = $this->_layerFactory->create();
			
			if (isset($data['layer_id']) && $id = $data['layer_id']) {
				$model->load($id);
            }
			
			$arrayData = $data;
			unset($arrayData['form_key']);		
			unset($arrayData['caption']);		
			
			if (isset($_FILES['image']) && isset($_FILES['image']['name']) && strlen($_FILES['image']['name'])) {
                /*
                 * Save image upload
                 */
                try {
                    $uploader = $this->_objectManager->create(
                        'Magento\MediaStorage\Model\File\Uploader',
                        ['fileId' => 'image']
                    );
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);

                    /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
                    $imageAdapter = $this->_objectManager->get('Magento\Framework\Image\AdapterFactory')->create();

                    $uploader->addValidateCallback('layer_image', $imageAdapter, 'validateUploadFile');
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);

                    /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                        ->getDirectoryRead(DirectoryList::MEDIA);
                    $result = $uploader->save(
                        $mediaDirectory->getAbsolutePath(\Neoxero\Revslider\Model\Layer::BASE_MEDIA_PATH)
                    );
                    $arrayData['image'] = \Neoxero\Revslider\Model\Layer::BASE_MEDIA_PATH.$result['file'];
                } catch (\Exception $e) {
                    if ($e->getCode() == 0) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
            } else {
                if (isset($data['image']) && isset($data['image']['value'])) {
                    if (isset($data['image']['delete'])) {
                        $arrayData['image'] = null;
                        $arrayData['delete_image'] = true;
                    } elseif (isset($data['image']['value'])) {
                        $arrayData['image'] = $data['image']['value'];
                    } else {
                        $arrayData['image'] = null;
                    }
                }
            }
			
			if (isset($_FILES['image_lazyload']) && isset($_FILES['image_lazyload']['name']) && strlen($_FILES['image_lazyload']['name'])) {
                /*
                 * Save image_lazyload upload
                 */
                try {
                    $uploader = $this->_objectManager->create(
                        'Magento\MediaStorage\Model\File\Uploader',
                        ['fileId' => 'image_lazyload']
                    );
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);

                    /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
                    $imageAdapter = $this->_objectManager->get('Magento\Framework\Image\AdapterFactory')->create();

                    $uploader->addValidateCallback('slide_image', $imageAdapter, 'validateUploadFile');
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);

                    /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                        ->getDirectoryRead(DirectoryList::MEDIA);
                    $result = $uploader->save(
                        $mediaDirectory->getAbsolutePath(\Neoxero\Revslider\Model\Slide::BASE_MEDIA_PATH)
                    );
                    $arrayData['image_lazyload'] = \Neoxero\Revslider\Model\Slide::BASE_MEDIA_PATH.$result['file'];
                } catch (\Exception $e) {
                    if ($e->getCode() == 0) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
            } else {
                if (isset($data['image_lazyload']) && isset($data['image_lazyload']['value'])) {
                    if (isset($data['image_lazyload']['delete'])) {
                        $arrayData['image_lazyload'] = null;
                        $arrayData['delete_image_lazyload'] = true;
                    } elseif (isset($data['image_lazyload']['value'])) {
                        $arrayData['image_lazyload'] = $data['image_lazyload']['value'];
                    } else {
                        $arrayData['image_lazyload'] = null;
                    }
                }
            }
			
			$data['params'] = json_encode($arrayData);
			$data['captions'] = json_encode($data['caption']);
            $model->setData($data);

            try {
                $model->save();

                $this->messageManager->addSuccess(__('The layer has been saved.'));
                $this->_getSession()->setFormData(false);

                if ($this->getRequest()->getParam('back') === 'edit') {
                    return $resultRedirect->setPath(
                        '*/*/edit',
                        [
                            'layer_id' => $model->getId(),
                            '_current' => true,
                            'current_slide_id' => $this->getRequest()->getParam('current_slide_id'),
                            'saveandclose' => $this->getRequest()->getParam('saveandclose'),
                        ]
                    );
                } elseif ($this->getRequest()->getParam('back') === 'new') {
                    return $resultRedirect->setPath(
                        '*/*/new',
                        ['_current' => TRUE]
                    );
                }

                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->messageManager->addException($e, __('Something went wrong while saving the layer.'));
            }

            $this->_getSession()->setFormData($data);

            return $resultRedirect->setPath(
                '*/*/edit',
                ['layer_id' => $this->getRequest()->getParam('layer_id')]
            );
        }

        return $resultRedirect->setPath('*/*/');
    }
}
