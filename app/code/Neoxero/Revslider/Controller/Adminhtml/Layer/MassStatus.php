<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Controller\Adminhtml\Layer;

/**
 * MassStatus action
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class MassStatus extends \Neoxero\Revslider\Controller\Adminhtml\Layer
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        $layerIds = $this->getRequest()->getParam('layer');
        $status = $this->getRequest()->getParam('status');

        if (!is_array($layerIds) || empty($layerIds)) {
            $this->messageManager->addError(__('Please select layer(s).'));
        } else {
            $layerCollection = $this->_layerCollectionFactory->create()
                ->addFieldToFilter('layer_id', ['in' => $layerIds]);
            try {
                foreach ($layerCollection as $layer) {
                    $layer->setStatus($status)
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been changed status.', count($layerIds))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        $resultRedirect = $this->_resultRedirectFactory->create();

        return $resultRedirect->setPath('*/*/');
    }
}
