<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Controller\Adminhtml\Layer;

/**
 * Delete Layer action
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Delete extends \Neoxero\Revslider\Controller\Adminhtml\Layer {

    public function execute() {
        $layerId = $this->getRequest()->getParam('layer_id');
        try {
            $layer = $this->_layerFactory->create()->setId($layerId);
            $layer->delete();
            $this->messageManager->addSuccess(
                    __('Delete successfully !')
            );
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }

        $resultRedirect = $this->_resultRedirectFactory->create();
        if ($this->getRequest()->getParam('back') === 'edit') {
            return $resultRedirect->setPath(
                            '*/slide/edit', [
                        'slide_id' => $this->getRequest()->getParam('current_slide_id'),
                        '_current' => true,
                        'saveandclose' => $this->getRequest()->getParam('saveandclose'),
                            ]
            );
        } else {
            return $resultRedirect->setPath('*/*/');
        }
    }

}
