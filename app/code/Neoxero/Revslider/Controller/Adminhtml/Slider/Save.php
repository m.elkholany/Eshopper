<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Controller\Adminhtml\Slider;

use Neoxero\Revslider\Model\Slider;

/**
 * Save Slider action
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Save extends \Neoxero\Revslider\Controller\Adminhtml\Slider
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        $resultRedirect = $this->_resultRedirectFactory->create();
        $formPostValues = $this->getRequest()->getPostValue();

        if (isset($formPostValues['slider'])) {
            $sliderData = $formPostValues['slider'];
            $sliderId = isset($sliderData['slider_id']) ? $sliderData['slider_id'] : null;

            $sliderData['params'] = json_encode($sliderData);

            $model = $this->_sliderFactory->create();

            $model->load($sliderId);

            $model->setData($sliderData);

            try {
                $model->save();

                if (isset($formPostValues['slider_slide'])) {
                    $slideGridSerializedInputData = $this->_jsHelper->decodeGridSerializedInput($formPostValues['slider_slide']);
                    $slideIds = [];
                    foreach ($slideGridSerializedInputData as $key => $value) {
                        $slideIds[] = $key;
                        $slideOrders[] = $value['order_slide_slider'];
                    }

                    $unSelecteds = $this->_slideCollectionFactory
                        ->create()
                        ->addFieldToFilter('slider_id', $model->getId());
                    if (count($slideIds)) {
                        $unSelecteds->addFieldToFilter('slide_id', array('nin' => $slideIds));
                    }

                    foreach ($unSelecteds as $slide) {
                        $slide->setSliderId(0)
                            ->setOrderSlide(0)->save();
                    }

                    $selectSlide = $this->_slideCollectionFactory
                        ->create()
                        ->addFieldToFilter('slide_id', array('in' => $slideIds));
                    $i = -1;
                    foreach ($selectSlide as $slide) {
                        $slide->setSliderId($model->getId())
                            ->setOrderSlide($slideOrders[++$i])->save();
                    }
                }

                $this->messageManager->addSuccess(__('The slider has been saved.'));
                $this->_getSession()->setFormData(false);

                if ($this->getRequest()->getParam('back') === 'edit') {
                    return $resultRedirect->setPath(
                        '*/*/edit', ['slider_id' => $model->getId(), '_current' => TRUE]
                    );
                } elseif ($this->getRequest()->getParam('back') === 'new') {
                    return $resultRedirect->setPath('*/*/new', ['_current' => TRUE]);
                }

                return $resultRedirect->setPath('*/*/');

            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->messageManager->addException($e, __('Something went wrong while saving the slider.'));
            }

            $this->_getSession()->setFormData($formPostValues);

            return $resultRedirect->setPath('*/*/edit', ['slider_id' => $sliderId]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
