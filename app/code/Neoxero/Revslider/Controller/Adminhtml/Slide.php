<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Controller\Adminhtml;

/**
 * Slide Abstract Action
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
abstract class Slide extends \Neoxero\Revslider\Controller\Adminhtml\AbstractAction
{
    /**
     * Check if admin has permissions to visit related pages.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Neoxero_Revslider::revslider_slides');
    }
}
