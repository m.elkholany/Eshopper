<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Neoxero.com license that is
 * available through the world-wide-web at this URL:
 * http://neoxero.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 * @copyright   Copyright (c) 2015 Neoxero (http://neoxero.com/)
 * @license     http://neoxero.com/license-agreement.html
 */

namespace Neoxero\Revslider\Controller\Adminhtml\Slide;

/**
 * MassStatus action
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class MassStatus extends \Neoxero\Revslider\Controller\Adminhtml\Slide
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        $slideIds = $this->getRequest()->getParam('slide');
        $status = $this->getRequest()->getParam('status');

        if (!is_array($slideIds) || empty($slideIds)) {
            $this->messageManager->addError(__('Please select slide(s).'));
        } else {
            $slideCollection = $this->_slideCollectionFactory->create()
                ->addFieldToFilter('slide_id', ['in' => $slideIds]);
            try {
                foreach ($slideCollection as $slide) {
                    $slide->setStatus($status)
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been changed status.', count($slideIds))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        $resultRedirect = $this->_resultRedirectFactory->create();

        return $resultRedirect->setPath('*/*/');
    }
}
