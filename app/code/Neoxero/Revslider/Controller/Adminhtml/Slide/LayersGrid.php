<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Neoxero.com license that is
 * available through the world-wide-web at this URL:
 * http://neoxero.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Controller\Adminhtml\Slide;

/**
 * layers Grid action
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class LayersGrid extends \Neoxero\Revslider\Controller\Adminhtml\Slide
{
    public function execute()
    {
        $resultLayout = $this->_resultLayoutFactory->create();
        $resultLayout->getLayout()->getBlock('revslider.slide.edit.tab.layers')
                     ->setInSlide($this->getRequest()->getPost('layer', null));

        return $resultLayout;
    }
}
