<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Controller\Adminhtml\Slide;

/**
 * MassDelete action.
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class MassDelete extends \Neoxero\Revslider\Controller\Adminhtml\Slide
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        $slideIds = $this->getRequest()->getParam('slide');
        if (!is_array($slideIds) || empty($slideIds)) {
            $this->messageManager->addError(__('Please select slide(s).'));
        } else {
            $slideCollection = $this->_slideCollectionFactory->create()
                ->addFieldToFilter('slide_id', ['in' => $slideIds]);
            try {
                foreach ($slideCollection as $slide) {
                    $slide->delete();
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been deleted.', count($slideIds))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        $resultRedirect = $this->_resultRedirectFactory->create();

        return $resultRedirect->setPath('*/*/');
    }
}
