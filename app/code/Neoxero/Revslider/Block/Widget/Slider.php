<?php
namespace Neoxero\Revslider\Block\Widget;
 
class Slider extends \Magento\Framework\View\Element\Template implements \Magento\Widget\Block\BlockInterface
{
	public function _toHtml()
    {
    	$this->setTemplate('widget/slider_base.phtml');
		return parent::_toHtml();
    }
}