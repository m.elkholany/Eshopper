<?php
/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Neoxero.com license that is
 * available through the world-wide-web at this URL:
 * http://neoxero.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Block\Widget;

class Sliderlist extends AbstractWidget
{


    /**
     * Slider Collection
     */
    protected $_sliderCollection;

	/**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Catalog\Helper\Category
     */
    protected $_revsliderHelper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context         
     * @param \Magento\Framework\Registry                      $registry        
     * @param \Neoxero\Revslider\Helper\Data                       $revsliderHelper     
     * @param \Neoxero\Revslider\Model\Slider                      $revsliderCollection 
     * @param array                                            $data            
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Neoxero\Revslider\Helper\Data $revsliderHelper,
        \Neoxero\Revslider\Model\Slider $sliderCollection,
        array $data = []
        ) {
        $this->_sliderCollection = $sliderCollection;
        $this->_revsliderHelper = $revsliderHelper;
        $this->_coreRegistry = $registry;
        parent::__construct($context, $revsliderHelper);
    }

    public function _toHtml()
    {
        $this->setTemplate('widget/revslider.phtml');
        return parent::_toHtml();
    }

    public function getSliderCollection()
    {
		$collection = $this->_sliderCollection->getCollection()
			->addFieldToFilter('status',1);
        return $collection;
    }
	
}