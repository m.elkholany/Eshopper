<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Block\Adminhtml\Slide\Edit\Tab;

use Neoxero\Revslider\Model\Status;

/**
 * Layers tab.
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Layers extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * Layer factory.
     *
     * @var \Neoxero\Revslider\Model\ResourceModel\Layer\CollectionFactory
     */
    protected $_layerCollectionFactory;
	
	/**
     * Slide factory.
     *
     * @var \Neoxero\Revslider\Model\ResourceModel\Slide\CollectionFactory
     */
    protected $_slideCollectionFactory;

    /**
     * [__construct description].
     *
     * @param \Magento\Backend\Block\Template\Context                         $context
     * @param \Magento\Backend\Helper\Data                                    $backendHelper
     * @param \Neoxero\Revslider\Model\ResourceModel\Layer\CollectionFactory $layerCollectionFactory
     * @param array                                                           $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Neoxero\Revslider\Model\ResourceModel\Layer\CollectionFactory $layerCollectionFactory,
        \Neoxero\Revslider\Model\ResourceModel\Slide\CollectionFactory $slideCollectionFactory,
        array $data = []
    ) {
        $this->_layerCollectionFactory = $layerCollectionFactory;
        $this->_slideCollectionFactory = $slideCollectionFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * _construct
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('layerGrid');
        $this->setDefaultSort('layer_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        if ($this->getRequest()->getParam('id')) {
            $this->setDefaultFilter(array('in_layer' => 1));
        }
    }

    /**
     * add Column Filter To Collection
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_layer') {
            $layerIds = $this->_getSelectedLayers();

            if (empty($layerIds)) {
                $layerIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('layer_id', array('in' => $layerIds));
            } else {
                if ($layerIds) {
                    $this->getCollection()->addFieldToFilter('layer_id', array('nin' => $layerIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }

        return $this;
    }

    /**
     * prepare collection
     */
    protected function _prepareCollection()
    {
        /** @var \Neoxero\Revslider\Model\ResourceModel\Layer\Collection $collection */
		$slideId = $this->getRequest()->getParam('slide_id');
        $collection = $this->_layerCollectionFactory->create();
		$collection->addFieldToFilter('main_table.slide_id', $slideId);
        $collection->setIsLoadSlideTitle(TRUE);

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_layer',
            [
                'header_css_class' => 'a-center',
                'type' => 'checkbox',
                'name' => 'in_layer',
                'align' => 'center',
                'index' => 'layer_id',
                'values' => $this->_getSelectedLayers(),
            ]
        );

        $this->addColumn(
            'layer_id',
            [
                'header' => __('Layer ID'),
                'type' => 'number',
                'index' => 'layer_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
            ]
        );
        $this->addColumn(
            'layer_name',
            [
                'header' => __('Name'),
                'index' => 'name',
				'filter_index' => 'main_table.name',
                'class' => 'xxx',
                'width' => '50px',
            ]
        );
		
		$this->addColumn(
            'type',
            [
                'header' => __('Type'),
                'index'   => 'type',
                'type'    => 'options',
				'filter_index' => 'main_table.type',
                'options' => $this->getTypeAvailableOption(),
                'class' => 'xxx',
                'width' => '50px',
            ]
        );
		
        $this->addColumn(
            'title',
            [
                'header' => __('Slide'),
                'index' => 'title',
                'type' => 'options',
                'filter_index' => 'main_table.slide_id',
                'options' => $this->getSlideAvailableOption(),
            ]
        );
        $this->addColumn(
            'layer_status',
            [
                'header' => __('Status'),
                'index' => 'status',
                'type' => 'options',
                'filter_index' => 'main_table.status',
                'options' => Status::getAvailableStatuses(),
            ]
        );

        $this->addColumn(
            'edit',
            [
                'header' => __('Edit'),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'header_css_class' => 'col-action',
                'column_css_class' => 'col-action',
                'renderer' => 'Neoxero\Revslider\Block\Adminhtml\Slide\Edit\Tab\Helper\Renderer\EditLayer',
            ]
        );

        /*$this->addColumn(
            'order_layer_slide',
            [
                'header' => __('Order'),
                'name' => 'order_layer_slide',
                'index' => 'order_layer_slide',
                'class' => 'xxx',
                'width' => '50px',
                'editable' => true,
            ]
        );*/

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/layersgrid', ['_current' => true]);
    }

    /**
     * get row url
     * @param  object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return '';
    }

    public function getSelectedSlideLayers()
    {
        $slideId = $this->getRequest()->getParam('slide_id');
        if (!isset($slideId)) {
            return [];
        }
        $layerCollection = $this->_layerCollectionFactory->create();
        $layerCollection->addFieldToFilter('slide_id', $slideId);

        $layerIds = [];
        foreach ($layerCollection as $layer) {
            $layerIds[$layer->getId()] = ['order_layer_slide' => $layer->getOrderLayer()];
        }

        return $layerIds;
    }

    protected function _getSelectedLayers()
    {
        $layers = $this->getRequest()->getParam('layer');
        if (!is_array($layers)) {
            $layers = array_keys($this->getSelectedSlideLayers());
        }

        return $layers;
    }
	
	/**
     * get slide vailable option
     *
     * @return array
     */
    public function getSlideAvailableOption()
    {
        $option = [];
        $slideCollection = $this->_slideCollectionFactory->create()->addFieldToSelect(['name']);

        foreach ($slideCollection as $slide) {
            $option[$slide->getId()] = $slide->getName();
        }

        return $option;
    }
	
	/**
     * get type vailable option
     *
     * @return array
     */
    public function getTypeAvailableOption()
    {
        $option = array(
			1 => 'Layer Text',
			2 => 'Layer Image',
			3 => 'Layer Video',
		);

        return $option;
    }

    /**
     * Prepare label for tab.
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('layers');
    }

    /**
     * Prepare title for tab.
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('layers');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return true;
    }
}
