<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Block\Adminhtml\Slide\Edit\Tab;

use Neoxero\Revslider\Model\Status;

/**
 * Image Edit tab.
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Image extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    protected $_objectFactory;

    /**
     * slider factory.
     *
     * @var \Neoxero\Revslider\Model\SliderFactory
     */
    protected $_sliderFactory;

    /**
     * @var \Neoxero\Revslider\Model\Slide
     */
    protected $_slide;
	
	/**
     * [$_revsliderHelper description].
     *
     * @var \Neoxero\Revslider\Helper\Data
     */
    protected $_revsliderHelper;
	
	/**
     * @var \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory
     */
    protected $_fieldFactory;

    /**
     * constructor.
     *
     * @param \Magento\Backend\Block\Template\Context                        $context
     * @param \Magento\Framework\Registry                                    $registry
     * @param \Magento\Framework\Data\FormFactory                            $formFactory
     * @param \Magento\Framework\DataObjectFactory                               $objectFactory
     * @param \Neoxero\Revslider\Model\Slide                           $slide
     * @param \Neoxero\Revslider\Model\SliderFactory                    $sliderFactory
	 * @param \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory $fieldFactory       [description]
     * @param array                                                          $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
		\Neoxero\Revslider\Helper\Data $revsliderHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Framework\DataObjectFactory $objectFactory,
        \Neoxero\Revslider\Model\Slide $slide,
        \Neoxero\Revslider\Model\SliderFactory $sliderFactory,
		\Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory $fieldFactory,
        array $data = []
    ) {
        $this->_objectFactory = $objectFactory;
        $this->_slide = $slide;
        $this->_sliderFactory = $sliderFactory;
		$this->_revsliderHelper = $revsliderHelper;
		$this->_fieldFactory = $fieldFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * prepare layout.
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('page.title')->setPageTitle($this->getPageTitle());

        \Magento\Framework\Data\Form::setFieldsetElementRenderer(
            $this->getLayout()->createBlock(
                'Neoxero\Revslider\Block\Adminhtml\Form\Renderer\Fieldset\Element',
                $this->getNameInLayout().'_fieldset_element'
            )
        );

        return $this;
    }

    /**
     * Prepare form.
     *
     * @return $this
     */
    protected function _prepareForm()
    {

        $dataObj = $this->_objectFactory->create();
        $model = $this->_coreRegistry->registry('slide');

        if ($sliderId = $this->getRequest()->getParam('current_slider_id')) {
            $model->setSliderId($sliderId);
        }

        $dataObj->addData($model->getData());

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
		
		/*
         * declare dependence
         */
        // dependence block
        $dependenceBlock = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Form\Element\Dependence'
        );
		
		// dependence field map array
        $fieldMaps = [];

        $form->setHtmlIdPrefix($this->_slide->getFormFieldHtmlIdPrefix());

        $fieldset = $form->addFieldset('image_fieldset', ['legend' => __('The Main Image')]);
		
		//Image Tab
        $fieldMaps['image'] = $fieldset->addField(
            'image',
            'image',
            [
                'title' => __('Background Image'),
                'label' => __('Background Image'),
                'name' => 'image',
                'note' => 'Allow image type: jpg, jpeg, gif, png',
            ]
        );

        $fieldMaps['image_alt'] = $fieldset->addField(
            'image_alt',
            'text',
            [
                'title' => __('Background Alt'),
                'label' => __('Background Alt'),
                'name' => 'image_alt',
                'note' => 'Used for SEO',
            ]
        );
		
		$fieldMaps['data_thumb'] = $fieldset->addField(
            'data_thumb',
            'image',
            [
                'title' => __('Slide Thumbnail'),
                'label' => __('Slide Thumbnail'),
                'name' => 'data_thumb',
                'note' => 'Allow image type: jpg, jpeg, gif, png <br> An Alternative Source for thumbs. If not defined a copy of the background image will be used in resized form',
            ]
        );
		
		$fieldMaps['data_lazyload'] = $fieldset->addField(
            'data_lazyload',
            'image',
            [
                'title' => __('Lazy Loading'),
                'label' => __('Lazy Loading'),
                'name' => 'data_lazyload',
                'note' => 'src="images/slides/dummy.jpg" (or any other dummy small image to decrease the loading time of Document) data-lazyload="path/filename" Here you need to declare the Path and File name of the image you wish to laod as Main Image in Slide',
            ]
        );
		
		$fieldMaps['data_bgfit'] = $fieldset->addField(
            'data_bgfit',
            'select',
            [
                'title' => __('Background Fit'),
                'label' => __('Background Fit'),
                'name' => 'data_bgfit',
                'values' => $this->_revsliderHelper->getBackgroundFit(),
				'note' => __('data-bgfit:cover / contain / normal / width(%) height(%) (select one to decide how the image should fit in the Slide Main Container).
				<br> For Ken Burn use only a Number, which is the % Zoom at start. 100 will fit with Width or height automatically, 200 will be double sized etc.. '),
            ]
        );	
		
		$fieldMaps['data_bgfit_x'] = $fieldset->addField(
            'data_bgfit_x',
            'text',
            [
                'title' => __('Background Fit X'),
                'label' => __('Background Fit X'),
                'name' => 'data_bgfit_x',
            ]
        );
		
		$fieldMaps['data_bgfit_y'] = $fieldset->addField(
            'data_bgfit_y',
            'text',
            [
                'title' => __('Background Fit Y'),
                'label' => __('Background Fit Y'),
                'name' => 'data_bgfit_y',
            ]
        );
		
		$fieldMaps['data_bgrepeat'] = $fieldset->addField(
            'data_bgrepeat',
            'select',
            [
                'title' => __('Background Repeat'),
                'label' => __('Background Repeat'),
                'name' => 'data_bgrepeat',
                'values' => $this->_revsliderHelper->getBackgroundRepeat(),
				'note' => __('data-bgrepeat:no-repeat / repeat / repeat-x / repeat-y (the way the image is shown in the slide main container)  '),
            ]
        );
		
		$fieldMaps['data_bgposition'] = $fieldset->addField(
            'data_bgposition',
            'select',
            [
                'title' => __('Background Position'),
                'label' => __('Background Position'),
                'name' => 'data_bgposition',
                'values' => $this->_revsliderHelper->getBackgroundPosition(),
				'note' => __('left top / left center / left bottom / center top / center center / center bottom / right top / right center / right bottom '),
            ]
        );
		
		$fieldMaps['data_bgposition_x'] = $fieldset->addField(
            'data_bgposition_x',
            'text',
            [
                'title' => __('Background Position X'),
                'label' => __('Background Position X'),
                'name' => 'data_bgposition_x',
            ]
        );
		
		$fieldMaps['data_bgposition_y'] = $fieldset->addField(
            'data_bgposition_y',
            'text',
            [
                'title' => __('Background Position Y'),
                'label' => __('Background Position Y'),
                'name' => 'data_bgposition_y',
            ]
        );
		
		
		$fieldMaps['data_kenburns'] = $fieldset->addField(
            'data_kenburns',
            'select',
            [
                'label' => __('Ken Burns Effect'),
                'title' => __('Ken Burns Effect'),
                'name' => 'data_kenburns',
                'values' => [
                    [
                        'value' => 'on',
                        'label' => __('On'),
                    ],
                    [
                        'value' => 'off',
                        'label' => __('Off'),
                    ],
                ],
				'note' => 'on/off to turn on Ken Burns Effect or keep it disabled.'
            ]
        ); 
		
		$fieldMaps['data_bgfitstart'] = $fieldset->addField(
            'data_bgfitstart',
            'text',
            [
                'title' => __('Background Fit Start'),
                'label' => __('Background Fit Start'),
                'name' => 'data_bgfitstart',
                'note' => 'Use only a Number . i.e. 300 will be a 300% Zoomed image where the basic 100% is fitting with width or height.',
            ]
        );
		
		$fieldMaps['data_bgfitend'] = $fieldset->addField(
            'data_bgfitend',
            'text',
            [
                'title' => __('Background Fit End'),
                'label' => __('Background Fit End'),
                'name' => 'data_bgfitend',
                'note' => 'Use only a Number . i.e. 300 will be a 300% Zoomed image where the basic 100% is fitting with width or height.',
            ]
        );
		
		$fieldMaps['data_bgpositionend'] = $fieldset->addField(
            'data_bgpositionend',
            'select',
            [
                'title' => __('Background Position End'),
                'label' => __('Background Position End'),
                'name' => 'data_bgpositionend',
                'values' => $this->_revsliderHelper->getBackgroundPosition(),
				'note' => __('left top / left center / left bottom / center top / center center / center bottom / right top / right center / right bottom For Ken Burns Animation. This is where the IMG will be animatied'),
            ]
        );
		
		$fieldMaps['data_bgpositionend_x'] = $fieldset->addField(
            'data_bgpositionend_x',
            'text',
            [
                'title' => __('Background End Position X'),
                'label' => __('Background End Position X'),
                'name' => 'data_bgpositionend_x',
            ]
        );
		
		$fieldMaps['data_bgpositionend_y'] = $fieldset->addField(
            'data_bgpositionend_y',
            'text',
            [
                'title' => __('Background End Position Y'),
                'label' => __('Background End Position Y'),
                'name' => 'data_bgpositionend_y',
            ]
        );
		
		$fieldMaps['data_ease'] = $fieldset->addField(
            'data_ease',
            'select',
            [
                'label' => __('Easing of Ken Burns Effect'),
                'title' => __('Easing of Ken Burns Effect'),
                'name' => 'data_ease',
                'options' => $this->_revsliderHelper->getDataEasing(),
				'note' => 'Same values as by Caption Easings available. The Movement Easing.'
            ]
        ); 
		
		$fieldMaps['data_duration'] = $fieldset->addField(
            'data_duration',
            'text',
            [
                'title' => __('Duration for Ken Burns'),
                'label' => __('Duration for Ken Burns'),
                'name' => 'data_duration',
                'note' => 'The value in ms how long the animation of ken burns effect should go. i.e. 3000 will make a 3s zoom and movement.',
            ]
        );
		
		$fieldMaps['css_image'] = $fieldset->addField(
            'css_image',
            'editor',
            [
                'name' => 'css_image',
                'label' => __('Custom CSS of Image'),
                'title' => __('Custom CSS of Image'),
                'wysiwyg' => false,
                'required' => false,
            ]
        );
		
		/*
         * Add field map
         */
        foreach ($fieldMaps as $fieldMap) {
            $dependenceBlock->addFieldMap($fieldMap->getHtmlId(), $fieldMap->getName());
        }

        $mappingFieldDependence = $this->getMappingFieldDependence();

        /*
         * Add field dependence
         */
        foreach ($mappingFieldDependence as $dependence) {
            $negative = isset($dependence['negative']) && $dependence['negative'];
            if (is_array($dependence['fieldName'])) {
                foreach ($dependence['fieldName'] as $fieldName) {
                    $dependenceBlock->addFieldDependence(
                        $fieldMaps[$fieldName]->getName(),
                        $fieldMaps[$dependence['fieldNameFrom']]->getName(),
                        $this->getDependencyField($dependence['refField'], $negative)
                    );
                }
            } else {
                $dependenceBlock->addFieldDependence(
                    $fieldMaps[$dependence['fieldName']]->getName(),
                    $fieldMaps[$dependence['fieldNameFrom']]->getName(),
                    $this->getDependencyField($dependence['refField'], $negative)
                );
            }
        }

        /*
         * add child block dependence
         */
        $this->setChild('form_after', $dependenceBlock);

        //$form->addValues($dataObj->getData());
		
		$slideData = $dataObj->getData();		
		if(isset($slideData['params'])) {
			$dataParams = json_decode($slideData['params']);
			$dataParams = (array)$dataParams;
			foreach($dataParams as $key => $value) {
				$slideData[$key] = $value;
			}
		} else {
			$slideData['data_kenburns'] = 'off';
		}
		
        $form->setValues($slideData);
        $this->setForm($form);

        return parent::_prepareForm();
    }
	
	/**
     * get dependency field.
     *
     * @return Magento\Config\Model\Config\Structure\Element\Dependency\Field [description]
     */
    public function getDependencyField($refField, $negative = false, $separator = ',', $fieldPrefix = '')
    {
        return $this->_fieldFactory->create(
            ['fieldData' => ['value' => (string)$refField, 'negative' => $negative, 'separator' => $separator], 'fieldPrefix' => $fieldPrefix]
        );
    }

    public function getMappingFieldDependence()
    {
        return [
			[
                'fieldName' => ['data_bgrepeat', 'data_bgfit', 'data_bgfit_x', 'data_bgfit_y'],
                'fieldNameFrom' => 'data_kenburns',
                'refField' => 'off',
            ],
			[
                'fieldName' => ['data_bgfitstart', 'data_bgfitend', 'data_bgpositionend', 'data_ease', 'data_duration'],
                'fieldNameFrom' => 'data_kenburns',
                'refField' => 'on',
            ],
			[
                'fieldName' => ['data_bgfit_x', 'data_bgfit_y'],
                'fieldNameFrom' => 'data_bgfit',
                'refField' => 'percentage',
            ],
			[
                'fieldName' => ['data_bgposition_x', 'data_bgposition_y'],
                'fieldNameFrom' => 'data_bgposition',
                'refField' => 'percentage',
            ],
			[
                'fieldName' => ['data_bgpositionend_x', 'data_bgpositionend_y'],
                'fieldNameFrom' => 'data_bgpositionend',
                'refField' => 'percentage',
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function getSlide()
    {
        return $this->_coreRegistry->registry('slide');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getPageTitle()
    {
        return $this->getSlide()->getId()
            ? __("Edit Slide '%1'", $this->escapeHtml($this->getSlide()->getName())) : __('New Slide');
    }

    /**
     * Prepare label for tab.
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('The Main Image');
    }

    /**
     * Prepare title for tab.
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('The Main Image');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
}
