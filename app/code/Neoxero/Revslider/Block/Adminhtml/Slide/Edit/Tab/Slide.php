<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *

 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Block\Adminhtml\Slide\Edit\Tab;

use Neoxero\Revslider\Model\Status;

/**
 * Slide Edit tab.
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Slide extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    protected $_objectFactory;

    /**
     * slider factory.
     *
     * @var \Neoxero\Revslider\Model\SliderFactory
     */
    protected $_sliderFactory;

    /**
     * @var \Neoxero\Revslider\Model\Slide
     */
    protected $_slide;
	
	/**
     * [$_revsliderHelper description].
     *
     * @var \Neoxero\Revslider\Helper\Data
     */
    protected $_revsliderHelper;
	
	/**
     * @var \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory
     */
    protected $_fieldFactory;

    /**
     * constructor.
     *
     * @param \Magento\Backend\Block\Template\Context                        $context
     * @param \Magento\Framework\Registry                                    $registry
     * @param \Magento\Framework\Data\FormFactory                            $formFactory
     * @param \Magento\Framework\DataObjectFactory                               $objectFactory
     * @param \Neoxero\Revslider\Model\Slide                           $slide
     * @param \Neoxero\Revslider\Model\SliderFactory                    $sliderFactory
	 * @param \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory $fieldFactory       [description]
     * @param array                                                          $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
		\Neoxero\Revslider\Helper\Data $revsliderHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Framework\DataObjectFactory $objectFactory,
        \Neoxero\Revslider\Model\Slide $slide,
        \Neoxero\Revslider\Model\SliderFactory $sliderFactory,
		\Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory $fieldFactory,
        array $data = []
    ) {
        $this->_objectFactory = $objectFactory;
        $this->_slide = $slide;
        $this->_sliderFactory = $sliderFactory;
		$this->_revsliderHelper = $revsliderHelper;
		$this->_fieldFactory = $fieldFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * prepare layout.
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('page.title')->setPageTitle($this->getPageTitle());

        \Magento\Framework\Data\Form::setFieldsetElementRenderer(
            $this->getLayout()->createBlock(
                'Neoxero\Revslider\Block\Adminhtml\Form\Renderer\Fieldset\Element',
                $this->getNameInLayout().'_fieldset_element'
            )
        );

        return $this;
    }

    /**
     * Prepare form.
     *
     * @return $this
     */
    protected function _prepareForm()
    {

        $dataObj = $this->_objectFactory->create();
        $model = $this->_coreRegistry->registry('slide');

        if ($sliderId = $this->getRequest()->getParam('current_slider_id')) {
            $model->setSliderId($sliderId);
        }

        $dataObj->addData($model->getData());

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
		
		/*
         * declare dependence
         */
        // dependence block
        $dependenceBlock = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Form\Element\Dependence'
        );
		
		// dependence field map array
        $fieldMaps = [];

        $form->setHtmlIdPrefix($this->_slide->getFormFieldHtmlIdPrefix());

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Slide Information')]);
        $fieldset2 = $form->addFieldset('custom_fieldset', ['legend' => __('Customs Style')]);

        if ($model->getId()) {
            $fieldset->addField('slide_id', 'hidden', ['name' => 'slide_id']);
        }

        $fieldMaps['name'] = $fieldset->addField(
            'name',
            'text',
            [
                'name' => 'name',
                'label' => __('Name'),
                'title' => __('Name'),
                'required' => true,
            ]
        );

        $fieldMaps['status'] = $fieldset->addField(
            'status',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Slide Status'),
                'name' => 'status',
                'options' => Status::getAvailableStatuses(),
            ]
        );

        $slider = $this->_sliderFactory->create()->load($sliderId);

        if ($slider->getId()) {
            $fieldMaps['slider_id'] = $fieldset->addField(
                'slider_id',
                'select',
                [
                    'label' => __('Slider'),
                    'name' => 'slider_id',
                    'values' => [
                        [
                            'value' => $slider->getId(),
                            'label' => $slider->getTitle(),
                        ],
                    ],
                ]
            );
        } else {
			if ($model->getId()) {
				$fieldMaps['slider_id'] = $fieldset->addField(
					'slider_id',
					'select',
					[
						'label' => __('Slider'),
						'name' => 'slider_id',
						'values' => $model->getAvailableSlides(),
						'disabled' => true,
					]
				);
			} else {
				$fieldMaps['slider_id'] = $fieldset->addField(
					'slider_id',
					'select',
					[
						'label' => __('Slider'),
						'name' => 'slider_id',
						'values' => $model->getAvailableSlides(),
					]
				);
			}
        }
		
		$fieldMaps['data_transition'] = $fieldset->addField(
            'data_transition',
            'select',
            [
                'label' => __('Data Transition'),
                'title' => __('Data Transition'),
                'name' => 'data_transition',
                'options' => Status::getDataTransition(),
				'note' => 'The appearance transition of this slide. You can define more than one, so in each loop the next slide transition type will be shown.'
            ]
        ); 
		
		$fieldMaps['data_randomtransition'] = $fieldset->addField(
            'data_randomtransition',
            'select',
            [
                'label' => __('Data Random Transition'),
                'title' => __('Data Random Transition'),
                'name' => 'data_randomtransition',
                'options' => Status::getAvailableOnOff(),
				'note' => '"on" / "off" - This will allow a Random transitions of the Selected Transitions you choosed in the data-transitions. Dont use together with random premium and random flat transitions !'
            ]
        ); 
		
		$fieldMaps['data_slotamount'] = $fieldset->addField(
            'data_slotamount',
            'text',
            [
                'title' => __('Data Slotamount'),
                'label' => __('Data Slotamount'),
                'name' => 'data_slotamount',
                'note' => 'The number of slots or boxes the slide is divided into. If you use boxfade, over 7 slots can be juggy.',
            ]
        );
		
		$fieldMaps['data_masterspeed'] = $fieldset->addField(
            'data_masterspeed',
            'text',
            [
                'title' => __('Data Masterspeed'),
                'label' => __('Data Masterspeed'),
                'name' => 'data_masterspeed',
                'note' => 'The speed of the transition in "ms".  default value is 300 (0.3 sec)',
            ]
        );
		
		$fieldMaps['data_delay'] = $fieldset->addField(
            'data_delay',
            'text',
            [
                'title' => __('Data Delay'),
                'label' => __('Data Delay'),
                'name' => 'data_delay',
                'note' => 'A new Dealy value for the Slide. If no delay defined per slide, the dealy defined via Options will be used',
            ]
        );
		
		$fieldMaps['data_saveperformance'] = $fieldset->addField(
            'data_saveperformance',
            'select',
            [
                'label' => __('Data Save Performance'),
                'title' => __('Data Save Performance'),
                'name' => 'data_saveperformance',
                'options' => Status::getAvailableOnOff(),
				'note' => '"on" / "off"'
            ]
        ); 

        $fieldMaps['data_link'] = $fieldset->addField(
            'data_link',
            'text',
            [
                'title' => __('Data Link'),
                'label' => __('Data Link'),
                'name' => 'data_link',
                'note' => 'A link on the whole slide pic',
            ]
        );

        $fieldMaps['data_target'] = $fieldset->addField(
            'data_target',
            'select',
            [
                'label' => __('Data Target'),
                'name' => 'data_target',
                'values' => [
					[
                        'value' => '',
                        'label' => '',
                    ],
                    [
                        'value' => '_self',
                        'label' => __('_self'),
                    ],
                    [
                        'value' => '_blank',
                        'label' => __('_blank'),
                    ],
                ],
				'note' => 'The target of the Link for the whole slide pic. (i.e. "_blank", "_self")',
            ]
        );
		
		$fieldMaps['data_title'] = $fieldset->addField(
            'data_title',
            'text',
            [
                'title' => __('Data Title'),
                'label' => __('Data Title'),
                'name' => 'data_title',
                'note' => 'In Navigation Style Preview1- preview4 mode you can show the Title of the Slides also. Due this option you can define for each slide its own Title',
            ]
        );
		
		// Custom Tab		
		$fieldMaps['id_attr'] = $fieldset2->addField(
            'id_attr',
            'text',
            [
                'title' => __('ID'),
                'label' => __('ID'),
                'name' => 'id_attr',
                'note' => 'Adds a unique ID to the li of the Slide like id="rev_special_id" (add only the id)',
            ]
        );
		
		$fieldMaps['class_attr'] = $fieldset2->addField(
            'class_attr',
            'text',
            [
                'title' => __('Classes'),
                'label' => __('Classes'),
                'name' => 'class_attr',
                'note' => 'Adds a unique class to the li of the Slide like class="rev_special_class" (add only the classnames, seperated by space)',
            ]
        );
		
		$fieldMaps['style_slide'] = $fieldset2->addField(
            'style_slide',
            'editor',
            [
                'name' => 'style_slide',
                'label' => __('Custom CSS'),
                'title' => __('Custom CSS'),
                'wysiwyg' => false,
                'required' => false,
            ]
        );
		
		$fieldMaps['order_slide'] = $fieldset2->addField(
            'order_slide',
            'text',
            [
                'title' => __('Order Slide'),
                'label' => __('Order Slide'),
                'name' => 'order_slide',
            ]
        );
		
		/*
         * Add field map
         */
        foreach ($fieldMaps as $fieldMap) {
            $dependenceBlock->addFieldMap($fieldMap->getHtmlId(), $fieldMap->getName());
        }

        $mappingFieldDependence = $this->getMappingFieldDependence();

        /*
         * Add field dependence
         */
        foreach ($mappingFieldDependence as $dependence) {
            $negative = isset($dependence['negative']) && $dependence['negative'];
            if (is_array($dependence['fieldName'])) {
                foreach ($dependence['fieldName'] as $fieldName) {
                    $dependenceBlock->addFieldDependence(
                        $fieldMaps[$fieldName]->getName(),
                        $fieldMaps[$dependence['fieldNameFrom']]->getName(),
                        $this->getDependencyField($dependence['refField'], $negative)
                    );
                }
            } else {
                $dependenceBlock->addFieldDependence(
                    $fieldMaps[$dependence['fieldName']]->getName(),
                    $fieldMaps[$dependence['fieldNameFrom']]->getName(),
                    $this->getDependencyField($dependence['refField'], $negative)
                );
            }
        }

        /*
         * add child block dependence
         */
        $this->setChild('form_after', $dependenceBlock);

        //$form->addValues($dataObj->getData());
		
		$slideData = $dataObj->getData();		
		if(isset($slideData['params'])) {
			$dataParams = json_decode($slideData['params']);
			$dataParams = (array)$dataParams;
			foreach($dataParams as $key => $value) {
				$slideData[$key] = $value;
			}
		} else {
			$slideData['data_transition'] = 'fade';
			$slideData['data_slotamount'] = 7;
			$slideData['data_masterspeed'] = 1500;
		}
		
        $form->setValues($slideData);
        $this->setForm($form);

        return parent::_prepareForm();
    }
	
	/**
     * get dependency field.
     *
     * @return Magento\Config\Model\Config\Structure\Element\Dependency\Field [description]
     */
    public function getDependencyField($refField, $negative = false, $separator = ',', $fieldPrefix = '')
    {
        return $this->_fieldFactory->create(
            ['fieldData' => ['value' => (string)$refField, 'negative' => $negative, 'separator' => $separator], 'fieldPrefix' => $fieldPrefix]
        );
    }

    public function getMappingFieldDependence()
    {
        return [
			
        ];
    }

    /**
     * @return mixed
     */
    public function getSlide()
    {
        return $this->_coreRegistry->registry('slide');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getPageTitle()
    {
        return $this->getSlide()->getId()
            ? __("Edit Slide '%1'", $this->escapeHtml($this->getSlide()->getName())) : __('New Slide');
    }

    /**
     * Prepare label for tab.
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Slide Information');
    }

    /**
     * Prepare title for tab.
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Slide Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
}
