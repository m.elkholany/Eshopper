<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *

 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Block\Adminhtml\Slide\Edit\Tab\Helper\Renderer;

/**
 * Edit layer form
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class EditLayer extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    /**
     * Store manager.
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * slide factory.
     *
     * @var \Neoxero\Revslider\Model\LayerFactory
     */
    protected $_layerFactory;

    /**
     * [__construct description].
     *
     * @param \Magento\Backend\Block\Context              $context       [description]
     * @param \Magento\Store\Model\StoreManagerInterface  $storeManager  [description]
     * @param \Neoxero\Revslider\Model\LayerFactory $layerFactory [description]
     * @param array                                       $data          [description]
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Neoxero\Revslider\Model\LayerFactory $layerFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_storeManager = $storeManager;
        $this->_layerFactory = $layerFactory;
    }

    /**
     * Render action.
     *
     * @param \Magento\Framework\DataObject $row
     *
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
        return "<a href=\"javascript:void(0);\" onclick=\"openLayerPopupWindow('". $this->getUrl('*/layer/edit', ['_current' => FALSE, 'layer_id' => $row->getId()]) ."')\">Edit</a>";
    }
}
