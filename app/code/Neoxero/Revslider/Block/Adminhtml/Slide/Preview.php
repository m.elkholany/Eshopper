<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Block\Adminhtml\Slide;

use Neoxero\Revslider\Model\Slide as SlideModel;
use Neoxero\Revslider\Model\Status;

/**
 * Preview Block
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Preview extends \Magento\Backend\Block\Template
{
    const STYLESLIDE_PARAM = 'slide_id';

	/**
     * template for slider.
     */
    const STYLESLIDE_REVOLUTION_TEMPLATE = 'Neoxero_Revslider::slide/preview.phtml';

    /**
     * slider factory.
     *
     * @var \Neoxero\Revslider\Model\SlideFactory
     */
    protected $_slideFactory;

    /**
     * slider model.
     *
     * @var \Neoxero\Revslider\Model\Slide
     */
    protected $_slide;

    /**
     * slide id.
     *
     * @var int
     */
    protected $_slideId;

    /**
     * revolution slider helper.
     *
     * @var \Neoxero\Revslider\Helper\Data
     */
    protected $_revsliderHelper;

    /**
     * @var \Neoxero\Revslider\Model\ResourceModel\Slide\CollectionFactory
     */
    protected $_slideCollectionFactory;
	
	/**
     * @var \Neoxero\Revslider\Model\ResourceModel\Layer\CollectionFactory
     */
    protected $_layerCollectionFactory;


    /**
     * [__construct description].
     *
     * @param \Magento\Backend\Block\Template\Context                $context
     * @param \Neoxero\Revslider\Model\ResourceModel\Slide\CollectionFactory $slideCollectionFactory
     * @param \Neoxero\Revslider\Model\SlideFactory                     $slideFactory
     * @param SliderModel $slider
     * @param \Neoxero\Revslider\Helper\Data                             $revsliderHelper
     * @param array                                                           $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Neoxero\Revslider\Model\ResourceModel\Slide\CollectionFactory $slideCollectionFactory,
        \Neoxero\Revslider\Model\ResourceModel\Layer\CollectionFactory $layerCollectionFactory,
        \Neoxero\Revslider\Model\SlideFactory $slideFactory,
        \Neoxero\Revslider\Model\layerFactory $layerFactory,
        SlideModel $slide,
        \Neoxero\Revslider\Helper\Data $revsliderHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_slideFactory = $slideFactory;
        $this->_layerFactory = $layerFactory;
        $this->_slide = $slide;
        $this->_revsliderHelper = $revsliderHelper;
        $this->_slideCollectionFactory = $slideCollectionFactory;
        $this->_layerCollectionFactory = $layerCollectionFactory;
    }

    /**
     * Add elements in layout.
     *
     * @return
     */
    protected function _prepareLayout()
    {
        $styleslideParam = $this->getRequest()->getParam(self::STYLESLIDE_PARAM);
		$this->setTemplate(self::STYLESLIDE_REVOLUTION_TEMPLATE);
		
        return parent::_prepareLayout();
    }

    /**
     * @return \Neoxero\Revslider\Model\Slide
     */
    public function getSlide()
    {
        if($this->getRequest()->getControllerName() == 'layer') {
			if(!empty($this->getRequest()->getParam('current_slide_id'))) {
				$this->_slideId = $this->getRequest()->getParam('current_slide_id');
			} else {
				$layerId = $this->getRequest()->getParam('layer_id');
				$layerCollection = $this->_layerFactory->create()->load($layerId);
				$this->_slideId = $layerCollection->getSlideId();
			}
		} else {
			$this->_slideId = $this->getRequest()->getParam(self::STYLESLIDE_PARAM);
		}
        $slide = $this->_slideFactory->create()->load($this->_slideId);
		return $slide;
    }

    /**
     * get slide image url.
     *
     * @param \Neoxero\Revslider\Model\Slide $slide
     *
     * @return string
     */
    public function getSlideImageUrl(\Neoxero\Revslider\Model\Slide $slide)
    {
        return $this->_revsliderHelper->getBaseUrlMedia($slide->getImage());
    }
	
	/**
     * get layer image url.
     *
     * @param \Neoxero\Revslider\Model\Layer $layer
     *
     * @return string
     */
    public function getLayerImageUrl(\Neoxero\Revslider\Model\Layer $layer)
    {
        $params = json_decode($layer->getParams());
		return $this->_revsliderHelper->getBaseUrlMedia($params->image);
    }
	
	/**
     * get layer image url.
     *
     * @param \Neoxero\Revslider\Model\Layer $layer
     *
     * @return string
     */
    public function getLayerImageLazyloadUrl(\Neoxero\Revslider\Model\Layer $layer)
    {
        $params = json_decode($layer->getParams());
		return $this->_revsliderHelper->getBaseUrlMedia($params->image_lazyload);
    }
	
	/**
     * get layer collection of slide.
     *
     * @return \Neoxero\Revslider\Model\ResourceModel\Layer\Collection
     */
    public function getLayerCollection($slideId)
    {

        /** @var \Neoxero\Revslider\Model\ResourceModel\Layer\Collection $layerCollection */
        $layerCollection = $this->_layerCollectionFactory->create()
            ->addFieldToFilter('slide_id', $slideId)
            ->addFieldToFilter('status', Status::STATUS_ENABLED)
            ->setOrder('order_layer', 'ASC');

        return $layerCollection;
    }
	
	/**
     * get all data of slide item.
     *
     * @return all data
     */
    public function getSlideItemData($slide)
    {
        $params = json_decode($slide->getParams()); 
		$arraySlideData = array();
		$arraySlideData['image'] = array();
		$arraySlideData['slide'] = array();
		
		//Slide Options
		if(!empty($params->data_transition)) {
			$arraySlideData['slide']['data-transition'] = $params->data_transition;
		}
		if(!empty($params->data_randomtransition)) {
			$arraySlideData['slide']['data-randomtransition'] = $params->data_randomtransition;
		}
		if(!empty($params->data_slotamount)) {
			$arraySlideData['slide']['data-slotamount'] = $params->data_slotamount;
		}
		if(!empty($params->data_masterspeed)) {
			$arraySlideData['slide']['data-masterspeed'] = $params->data_masterspeed;
		}
		if(!empty($params->data_delay)) {
			$arraySlideData['slide']['data-delay'] = $params->data_delay;
		}
		if(!empty($params->data_link)) {
			$arraySlideData['slide']['data-link'] = $params->data_link;
		}
		if(!empty($params->data_target)) {
			$arraySlideData['slide']['data-target'] = $params->data_target;
		}
		if(!empty($params->data_slideindex)) {
			$arraySlideData['slide']['data-slideindex'] = $params->data_slideindex;
		}
		if(!empty($params->data_thumb)) {
			$arraySlideData['slide']['data-thumb'] = $this->_revsliderHelper->getBaseUrlMedia($params->data_thumb);
		}
		if(!empty($params->data_saveperformance)) {
			$arraySlideData['slide']['data-saveperformance'] = $params->data_saveperformance;
		}
		if(!empty($params->data_title)) {
			$arraySlideData['slide']['data-title'] = $params->data_title;
		}
		
		// Image Attribute
		if(!empty($params->data_lazyload)) {
			$arraySlideData['image']['data-lazyload'] = $this->_revsliderHelper->getBaseUrlMedia($params->data_lazyload);
		}		
		if(!empty($params->data_bgposition)) {
			if($params->data_bgposition == 'percentage') {
				$arraySlideData['image']['data-bgposition'] = '('.$params->data_bgposition_x .'% '. $params->data_bgposition_y .'%)';
			} else {
				$arraySlideData['image']['data-bgposition'] = $params->data_bgposition;
			}
		}
		
		if(!empty($params->data_kenburns) && $params->data_kenburns == 'off') {
			if(!empty($params->data_bgrepeat)) {
				$arraySlideData['image']['data-bgrepeat'] = $params->data_bgrepeat;
			}
			if(!empty($params->data_bgfit)) {
				if($params->data_bgfit == 'percentage') {
					$arraySlideData['image']['data-bgfit'] = '('.$params->data_bgfit_x .'% '. $params->data_bgfit_y .'%)';
				} else {
					$arraySlideData['image']['data-bgfit'] = $params->data_bgfit;
				}
			}
		} else {
			if(!empty($params->data_kenburns)) {
				$arraySlideData['image']['data-kenburns'] = $params->data_kenburns;
			}
			if(!empty($params->data_duration)) {
				$arraySlideData['image']['data-duration'] = $params->data_duration;
			}
			if(!empty($params->data_ease)) {
				$arraySlideData['image']['data-ease'] = $params->data_ease;
			}
			if(!empty($params->data_bgfitstart)) {
				$arraySlideData['image']['data-bgfit'] = $params->data_bgfitstart;
			}
			if(!empty($params->data_bgfitend)) {
				$arraySlideData['image']['data-bgfitend'] = $params->data_bgfitend;
			}
			if(!empty($params->data_bgpositionend)) {
				$arraySlideData['image']['data-bgpositionend'] = $params->data_bgpositionend;
			}
			if(!empty($params->css_image)) {
				$arraySlideData['image']['css_image'] = $params->css_image;
			}
		}
		
        return $arraySlideData;
	}

	/**
     * get all data of layer item.
     *
     * @return all data
     */
    public function getLayerItemData($layer)
    {
        $params = json_decode($layer->getParams());
		$captions = json_decode($layer->getCaptions());
		
		$arrayLayerData = array();
		$arrayLayerData['params'] = array();
		$arrayLayerData['captions'] = array();
		
		//Caption Classes
		$class[] = 'tp-caption';
		if(!empty($captions->incomingclasses)) {
			$class[] = $captions->incomingclasses;
		}
		if(!empty($captions->outgoingclasses)) {
			$class[] = $captions->outgoingclasses;
		}
		if(!empty($params->text_style)) {
			$class[] = $params->text_style;
		}
		if(!empty($captions->class_attr)) {
			$class[] = $captions->class_attr;
		}
		$arrayLayerData['params']['class'] = implode(' ', $class);
		
		
		// Caption data - settings
		if(!empty($captions->left)) {
			$arrayLayerData['captions']['data-x'] = $captions->left;
		}
		if(!empty($captions->top)) {
			$arrayLayerData['captions']['data-y'] = $captions->top;
		}
		if(!empty($captions->data_hoffset)) {
			$arrayLayerData['captions']['data-hoffset'] = $captions->data_hoffset;
		}
		if(!empty($captions->data_voffset)) {
			$arrayLayerData['captions']['data-voffset'] = $captions->data_voffset;
		}
		if(!empty($captions->data_speed)) {
			$arrayLayerData['captions']['data-speed'] = $captions->data_speed;
		}
		if(!empty($captions->data_splitin)) {
			$arrayLayerData['captions']['data-splitin'] = $captions->data_splitin;
		}
		if(!empty($captions->data_elementdelay)) {
			$arrayLayerData['captions']['data-elementdelay'] = $captions->data_elementdelay;
		}
		if(!empty($captions->data_splitout)) {
			$arrayLayerData['captions']['data-splitout'] = $captions->data_splitout;
		}
		if(!empty($captions->data_endelementdelay)) {
			$arrayLayerData['captions']['data-endelementdelay'] = $captions->data_endelementdelay;
		}
		if(!empty($captions->data_start)) {
			$arrayLayerData['captions']['data-start'] = $captions->data_start;
		}
		if(!empty($captions->data_easing)) {
			$arrayLayerData['captions']['data-easing'] = $captions->data_easing;
		}
		if(!empty($captions->data_endspeed)) {
			$arrayLayerData['captions']['data-endspeed'] = $captions->data_endspeed;
		}
		if(!empty($captions->data_end)) {
			$arrayLayerData['captions']['data-end'] = $captions->data_end;
		}
		if(!empty($captions->data_endeasing)) {
			$arrayLayerData['captions']['data-endeasing'] = $captions->data_endeasing;
		}
		
		if(!empty($captions->incomingclasses)) {
			$arrayLayerData['captions']['data-customin'] = $captions->customin;
		}
		if(!empty($captions->outgoingclasses)) {
			$arrayLayerData['captions']['data-customout'] = $captions->customout;
		}
		
		// Media in Slide
		if($params->type == 3) {
			if(!empty($captions->data_autoplay)) {
				$arrayLayerData['captions']['data-autoplay'] = $captions->data_autoplay;
			}
			if(!empty($captions->data_autoplayonlyfirsttime)) {
				$arrayLayerData['captions']['data-autoplayonlyfirsttime'] = $captions->data_autoplayonlyfirsttime;
			}
			if(!empty($captions->data_nextslideatend)) {
				$arrayLayerData['captions']['data-nextslideatend'] = $captions->data_nextslideatend;
			}
			if(!empty($captions->data_videoposter) && ($captions->data_autoplay == 'false' || $captions->data_autoplayonlyfirsttime == 'true')) {
				$arrayLayerData['captions']['data-videoposter'] = $captions->data_videoposter;
			}			
			if(!empty($captions->data_forcerewind)) {
				$arrayLayerData['captions']['data-forcerewind'] = $captions->data_forcerewind;
			}
			if(!empty($captions->data_volume)) {
				$arrayLayerData['captions']['data-volume'] = $captions->data_volume;
			}
			if(!empty($captions->video_width)) {
				$arrayLayerData['captions']['data-videowidth'] = $captions->video_width;
			}
			if(!empty($captions->video_height)) {
				$arrayLayerData['captions']['data-videoheight'] = $captions->video_height;
			}
			if(!empty($captions->data_aspectratio)) {
				$arrayLayerData['captions']['data-aspectratio'] = $captions->data_aspectratio;
			}
			if(!empty($captions->data_videopreload)) {
				$arrayLayerData['captions']['data-videopreload'] = $captions->data_videopreload;
			}
			if($params->video_type == 'html5') {
				if(!empty($captions->data_forcecover)) {
					$arrayLayerData['captions']['data-forcecover'] = $captions->data_forcecover;
				}
				if(!empty($captions->data_videomp4)) {
					$arrayLayerData['captions']['data-videomp4'] = $captions->data_videomp4;
				}
				if(!empty($captions->data_videowebm)) {
					$arrayLayerData['captions']['data-videowebm'] = $captions->data_videowebm;
				}
				if(!empty($captions->data_videoogv)) {
					$arrayLayerData['captions']['data-videoogv'] = $captions->data_videoogv;
				}
				if(!empty($captions->data_videoloop)) {
					$arrayLayerData['captions']['data-videoloop'] = $captions->data_videoloop;
				}
			}
			if(!empty($captions->data_videocontrols)) {
				$arrayLayerData['captions']['data-videocontrols'] = $captions->data_videocontrols;
			}
		}
		
		if(!empty($captions->custom_css)) {
			$arrayLayerData['captions']['style'] = $captions->custom_css;
		}
		
		switch ($params->type) {
			case 1:
				if (!empty($captions->link)) {
					$arrayLayerData['content'] = '<a href="'.$captions->link.'">'. $params->text . '</a>';
				} else {
					$arrayLayerData['content'] = $params->text;
				}
				break;
			case 2:
				$arrayAttrImage = array();
				if(!empty($params->image_lazyload)) {
					$arrayAttrImage['data-lazyload'] = $this->getLayerImageLazyloadUrl($layer);
				} 
				if(!empty($params->image_width)) {
					$arrayAttrImage['data-ww'] = $params->image_width;
				} 
				if(!empty($params->image_height)) {
					$arrayAttrImage['data-hh'] = $params->image_height;
				}
				
				$attrImage = '';
				foreach($arrayAttrImage as $key=>$attr) {
					$attrImage .= $key. '="'. $attr . '" ';
				} 
				
				if (!empty($captions->link)) {
					$arrayLayerData['content'] = '<a href="'.$captions->link.'"><img src="'. $this->getLayerImageUrl($layer) .'" alt="'. $params->image_alt .'" '.$attrImage.'></a>';
				} else {
					$arrayLayerData['content'] = '<img src="'. $this->getLayerImageUrl($layer) .'" alt="'. $params->image_alt .'" '.$attrImage.'>';
				}
				break;
			case 3:
				if ($params->video_type == 'vimeo') {
					$arrayLayerData['content'] = "<iframe width=\"{$params->video_width}\" height=\"{$params->video_height}\" frameborder=\"0\" src=\"http://player.vimeo.com/video/{$params->video_id}?title=0&amp;byline=0&amp;portrait=0;api=1\"></iframe>";
				} elseif ($params->video_type == 'youtube') {
					$arrayLayerData['content'] = "<iframe width=\"{$params->video_width}\" height=\"{$params->video_height}\" src=\"http://www.youtube.com/embed/{$params->video_id}?hd=1&amp;wmode=opaque&amp;controls=1&amp;showinfo=0\" frameborder=\"0\" allowfullscreen></iframe>";
				} else {
					$arrayLayerData['content'] = $params->html5;
				}
		}
		
        return $arrayLayerData;
	}

}
