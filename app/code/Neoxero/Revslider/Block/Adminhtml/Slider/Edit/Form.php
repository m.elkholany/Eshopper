<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Block\Adminhtml\Slider\Edit;

/**
 * Adminhtml locator edit form block.
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            array(
                'data' => array(
                    'id' => 'edit_form',
                    'action' => $this->getUrl('*/*/save'),
                    'method' => 'post',
                    'enctype' => 'multipart/form-data',
                ),
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
