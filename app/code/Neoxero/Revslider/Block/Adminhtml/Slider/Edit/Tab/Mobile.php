<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Block\Adminhtml\Slider\Edit\Tab;

use Neoxero\Revslider\Model\Status;

/**
 * Slider Form.
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Mobile extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    const FIELD_NAME_SUFFIX = 'slider';

    /**
     * @var \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory
     */
    protected $_fieldFactory;

    /**
     * [$_revsliderHelper description].
     *
     * @var \Neoxero\Revslider\Helper\Data
     */
    protected $_revsliderHelper;

    /**
     * [__construct description].
     *
     * @param \Magento\Backend\Block\Template\Context                                $context            [description]
     * @param \Neoxero\Revslider\Helper\Data                                    $revsliderHelper [description]
     * @param \Magento\Framework\Registry                                            $registry           [description]
     * @param \Magento\Framework\Data\FormFactory                                    $formFactory        [description]
     * @param \Magento\Store\Model\System\Store                                      $systemStore        [description]
     * @param \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory $fieldFactory       [description]
     * @param array                                                                  $data               [description]
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Neoxero\Revslider\Helper\Data $revsliderHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory $fieldFactory,
        array $data = []
    ) {
        $this->_revsliderHelper = $revsliderHelper;
        $this->_fieldFactory = $fieldFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('page.title')->setPageTitle($this->getPageTitle());
    }

    /**
     * Prepare form.
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $slider = $this->getSlider();
        $isElementDisabled = true;
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        // dependence field map array
        $fieldMaps = [];

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Mobile Visibility')]);

        $fieldset->addField(
            'hideCaptionAtLimit',
            'text',
            [
                'name' => 'hideCaptionAtLimit',
                'label' => __('Hide Caption At Limit'),
                'title' => __('Hide Caption At Limit'),
				'note' => __('It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)'),
            ]
        );

        $fieldset->addField(
            'hideAllCaptionAtLimit',
            'text',
            [
                'name' => 'hideAllCaptionAtLimit',
                'label' => __('Hide All Caption At Limit'),
                'title' => __('Hide All Caption At Limit'),
				'note' => __('Hide all The Captions if Width of Browser is less then this value'),
            ]
        );
		
		$fieldset->addField(
            'hideSliderAtLimit',
            'text',
            [
                'name' => 'hideSliderAtLimit',
                'label' => __('Hide Slider At Limit'),
                'title' => __('Hide Slider At Limit'),
				'note' => __('Hide the whole slider, and stop also functions if Width of Browser is less than this value'),
            ]
        );

        $fieldset->addField(
            'hideNavDelayOnMobile',
            'text',
            [
                'name' => 'hideNavDelayOnMobile',
                'label' => __('Hide Nav Delay On Mobile'),
                'title' => __('Hide Nav Delay On Mobile'),
				'note' => __('Hide all navigation after a while on Mobile (touch and release events based)'),
            ]
        );
		
		$fieldset->addField(
            'hideThumbsOnMobile',
            'select',
            [
                'name' => 'hideThumbsOnMobile',
                'label' => __('Hide Thumbs On Mobile'),
                'title' => __('Hide Thumbs On Mobile'),
				'options' => Status::getAvailableOnOff(),
				'note' => __('Possible Values: "on", "off" - if set to "on", Thumbs are not shown on Mobile Devices'),
            ]
        );
		
		$fieldset->addField(
            'hideBulletsOnMobile',
            'select',
            [
                'name' => 'hideBulletsOnMobile',
                'label' => __('Hide Bullets On Mobile'),
                'title' => __('Hide Bullets On Mobile'),
				'options' => Status::getAvailableOnOff(),
				'note' => __('Possible Values: "on", "off" - if set to "on", Bullets are not shown on Mobile Devices'),
            ]
        );
		
		$fieldset->addField(
            'hideArrowsOnMobile',
            'select',
            [
                'name' => 'hideArrowsOnMobile',
                'label' => __('Hide Arrows On Mobile'),
                'title' => __('Hide Arrows On Mobile'),
				'options' => Status::getAvailableOnOff(),
				'note' => __('Possible Values: "on", "off" - if set to "on", Arrows are not shown on Mobile Devices'),
            ]
        );
		
		$fieldset->addField(
            'hideThumbsUnderResoluition',
            'text',
            [
                'name' => 'hideThumbsUnderResoluition',
                'label' => __('Hide Thumbs Under Resoluition'),
                'title' => __('Hide Thumbs Under Resoluition'),
				'note' => __('Possible Values: 0 - 1900 - defines under which resolution the Thumbs should be hidden. (only if hideThumbonMobile set to off)'),
            ]
        );

        $sliderData = $slider->getData();		
		if(isset($sliderData['params'])) {
			$data = json_decode($sliderData['params']);
			$data = (array)$data;
		} else {
			$data = $sliderData;
		}
		
        $form->setValues($data);
        $form->addFieldNameSuffix(self::FIELD_NAME_SUFFIX);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    public function getSlider()
    {
        return $this->_coreRegistry->registry('slider');
    }

    public function getPageTitle()
    {
        return $this->getSlider()->getId() ? __("Edit Slider '%1'", $this->escapeHtml($this->getSlider()->getTitle())) : __('New Slider');
    }

    /**
     * Prepare label for tab.
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Mobile Visibility');
    }

    /**
     * Prepare title for tab.
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Mobile Visibility');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
}
