<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Block\Adminhtml\Slider;

/**
 * Slider block edit form container.
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry.
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @param Context $context
     * @param array   $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }
    protected function _construct()
    {
        $this->_objectId = 'slider_id';
        $this->_blockGroup = 'Neoxero_Revslider';
        $this->_controller = 'adminhtml_slider';

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save Slider'));
        $this->buttonList->update('delete', 'label', __('Delete'));

        if ($this->getSlider()->getId()) {
            $this->buttonList->add(
                'create_slide',
                [
                    'label' => __('Create Slide'),
                    'class' => 'add',
                    'onclick' => 'openSlidePopupWindow(\''.$this->getCreateSlideUrl().'\')',
                ],
                1
            );
        }

        $this->buttonList->add(
            'save_and_continue',
            [
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                    ],
                ],
            ],
            10
        );

        /*
         * See more at file magento2root/lib/web/mage/adminhtml/grid.js
         */
        $this->_formScripts[] = "
			require(['jquery'], function($){
				window.openSlidePopupWindow = function (url) {
					var left = ($(document).width()-1000)/2, height= $(document).height();
					var create_slide_popupwindow = window.open(url, '_blank','width=1000,resizable=1,scrollbars=1,toolbar=1,'+'left='+left+',height='+height);
					var windowFocusHandle = function(){
						if (create_slide_popupwindow.closed) {
							if (typeof slideGridJsObject !== 'undefined' && create_slide_popupwindow.slide_id) {
								slideGridJsObject.reloadParams['slide[]'].push(create_slide_popupwindow.slide_id + '');
								$(edit_form.slider_slide).val($(edit_form.slider_slide).val() + '&' + create_slide_popupwindow.slide_id + '=' + Base64.encode('order_slide_slider=0'));
				       			slideGridJsObject.setPage(create_slide_popupwindow.slide_id);
				       		}
				       		$(window).off('focus',windowFocusHandle);
						} else {
							$(create_slide_popupwindow).trigger('focus');
							create_slide_popupwindow.alert('".__('You have to save slide and close this window!')."');
						}
					}
					$(window).focus(windowFocusHandle);
				}
			});
		";
    }

    public function getSlider()
    {
        return $this->_coreRegistry->registry('slider');
    }

    /**
     * Retrieve the save and continue edit Url.
     *
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl(
            '*/*/save',
            ['_current' => true, 'back' => 'edit', 'tab' => '{{tab_id}}']
        );
    }

    /**
     * get create slide url.
     *
     * @return string
     */
    public function getCreateSlideUrl()
    {
        return $this->getUrl('*/slide/new', ['current_slider_id' => $this->getSlider()->getId()]);
    }
}
