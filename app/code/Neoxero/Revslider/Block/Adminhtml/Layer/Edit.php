<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Block\Adminhtml\Layer;

/**
 * Layer block edit form container.
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * _construct
     * @return void
     */
    protected function _construct()
    {
		$this->_objectId = 'layer_id';
        $this->_blockGroup = 'Neoxero_Revslider';
        $this->_controller = 'adminhtml_layer';		

        parent::_construct();

        $this->buttonList->remove('save');
		$this->buttonList->remove('back');
		$this->buttonList->remove('delete');
		
		$this->buttonList->add(
            'close_window',
            [
                'label' => __('Close Window'),
                'onclick' => 'window.close();',
            ],
            10
        );
		
		$this->buttonList->add(
            'delete',
            [
                'label' => __('Delete'),
                'class' => 'delete',
                'onclick' => 'customDelete()',
            ],
            10
        );

        $this->buttonList->add(
            'save_and_continue',
            [
                'label' => __('Save and Preview'),
                'class' => 'save',
                'onclick' => 'customsaveAndContinueEdit()',
            ],
            10
        );

        $this->buttonList->add(
            'save_and_close',
            [
                'label' => __('Save and Close'),
                'class' => 'save_and_close',
                'onclick' => 'saveAndCloseWindow()',
            ],
            10
        );

        $this->_formScripts[] = "
			require(['jquery'], function($){
				$(document).ready(function(){
					var input = $('<input class=\"custom-button-submit\" type=\"submit\" hidden=\"true\" />');
					$(edit_form).append(input);

					window.customsaveAndContinueEdit = function (){
						edit_form.action = '".$this->getSaveAndContinueUrl()."';
						$('.custom-button-submit').trigger('click');

			        }

		    		window.saveAndCloseWindow = function (){
		    			edit_form.action = '".$this->getSaveAndCloseWindowUrl()."';
						$('.custom-button-submit').trigger('click');
		            }
					
					window.customDelete = function (){
		    			edit_form.action = '".$this->getDeleteAndCloseWindowUrl()."';
						$('.custom-button-submit').trigger('click');
		            }
				});
			});
		";

        if ($layerId = $this->getRequest()->getParam('layer_id')) {
            $this->_formScripts[] = '
				window.layer_id = '.$layerId.';
			';
        }
		
        if (!$this->getRequest()->getParam('layer_id')) {            
            $this->buttonList->remove('delete');            
        } 

        if ($this->getRequest()->getParam('saveandclose')) {
            $this->_formScripts[] = 'window.close();';
        }
    }

    /**
     * Retrieve the save and continue edit Url.
     *
     * @return string
     */
    protected function getSaveAndContinueUrl()
    {
        return $this->getUrl(
            '*/*/save',
            [
                '_current' => true,
				'back' => 'edit',
                'layer_id' => $this->getRequest()->getParam('layer_id'),
				'current_slide_id' => $this->getRequest()->getParam('current_slide_id'),
            ]
        );
    }

    /**
     * Retrieve the save and continue edit Url.
     *
     * @return string
     */
    protected function getSaveAndCloseWindowUrl()
    {
        return $this->getUrl(
            '*/*/save',
            [
                '_current' => true,
				'back' => 'edit',
                'layer_id' => $this->getRequest()->getParam('layer_id'),
				'current_slide_id' => $this->getRequest()->getParam('current_slide_id'),
                'saveandclose' => 1,
            ]
        );
    }
	
	/**
     * Retrieve the save and continue edit Url.
     *
     * @return string
     */
    protected function getDeleteAndCloseWindowUrl()
    {
        return $this->getUrl(
            '*/*/delete',
            [
                '_current' => true,
				'back' => 'edit',
                'layer_id' => $this->getRequest()->getParam('layer_id'),
				'current_slide_id' => $this->getRequest()->getParam('current_slide_id'),
                'saveandclose' => 1,
            ]
        );
    }
}
