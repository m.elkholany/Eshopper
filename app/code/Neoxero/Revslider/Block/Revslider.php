<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Block;

use Neoxero\Revslider\Model\Slider as SliderModel;
use Neoxero\Revslider\Model\Status;

/**
 * Revslider Block
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Revslider extends \Magento\Framework\View\Element\Template
{
    /**
     * revolution slider template
     * @var string
     */
    protected $_template = 'Neoxero_Revslider::revslider.phtml';

    /**
     * Registry object.
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * slider collecion factory.
     *
     * @var \Neoxero\Revslider\Model\ResourceModel\Slider\CollectionFactory
     */
    protected $_sliderCollectionFactory;

    /**
     * scope config.
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * [__construct description].
     *
     * @param \Magento\Framework\View\Element\Template\Context                $context
     * @param \Magento\Framework\Registry                                     $coreRegistry
     * @param \Neoxero\Revslider\Model\ResourceModel\Slider\CollectionFactory $sliderCollectionFactory
     * @param \Neoxero\Revslider\Model\ResourceModel\Slide\CollectionFactory $slideCollectionFactory
     * @param \Magento\Catalog\Model\CategoryFactory                          $categoryFactory
     * @param \Magento\Store\Model\StoreManagerInterface                      $storeManager
     * @param array                                                           $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Neoxero\Revslider\Model\ResourceModel\Slider\CollectionFactory $sliderCollectionFactory,
        \Neoxero\Revslider\Model\ResourceModel\Slide\CollectionFactory $slideCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_coreRegistry = $coreRegistry;
        $this->_sliderCollectionFactory = $sliderCollectionFactory;

        $this->_scopeConfig = $context->getScopeConfig();
        $this->_storeManager = $context->getStoreManager();
    }

    /**
     * @return
     */
    protected function _toHtml()
    {
        $store = $this->_storeManager->getStore()->getId();

        if ($this->_scopeConfig->getValue(
            SliderModel::XML_CONFIG_REVSLIDER, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store
        )
        ) {
            return parent::_toHtml();
        }

        return '';
    }

    /**
     * add child block slider.
     *
     * @param \Neoxero\Revslider\Model\ResourceModel\Slider\Collection $sliderCollection [description]
     *
     * @return \Neoxero\Revslider\Block\Revslider [description]
     */
    public function appendChildBlockSliders(
        \Neoxero\Revslider\Model\ResourceModel\Slider\Collection $sliderCollection
    ) {
        foreach ($sliderCollection as $slider) {
            $this->append(
                $this->getLayout()->createBlock(
                    'Neoxero\Revslider\Block\SliderItem'
                )->setSliderId($slider->getId())
            );
        }

        return $this;
    }

}
