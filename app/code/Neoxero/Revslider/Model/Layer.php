<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Model;

/**
 * Layer Model
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Layer extends \Magento\Framework\Model\AbstractModel
{
    const BASE_MEDIA_PATH = 'neoxero/revslider/layer';

    const SLIDE_TARGET_SELF = 1;
    const SLIDE_TARGET_BLANK = 2;

    /**
     * Layer colleciton factory.
     *
     * @var \Neoxero\Revslider\Model\ResourceModel\Layer\CollectionFactory
     */
    protected $_slideCollectionFactory;

    /**
     * Layer factory.
     *
     * @var \Neoxero\Revslider\Model\layerFactory
     */
    protected $_layerFactory;

    /**
     * [$_formFieldHtmlIdPrefix description].
     *
     * @var string
     */
    protected $_formFieldHtmlIdPrefix = 'page_';

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * logger.
     *
     * @var \Magento\Framework\Logger\Monolog
     */
    protected $_monolog;

    /**
     * [__construct description].
     *
     * @param \Magento\Framework\Model\Context                                $context
     * @param \Magento\Framework\Registry                                     $registry
     * @param \Neoxero\Revslider\Model\ResourceModel\Layer                   $resource
     * @param \Neoxero\Revslider\Model\ResourceModel\Layer\Collection        $resourceCollection
     * @param \Neoxero\Revslider\Model\LayerFactory                     $layerFactory
     * @param \Neoxero\Revslider\Model\ResourceModel\Layer\CollectionFactory $slideCollectionFactory
     * @param \Magento\Store\Model\StoreManagerInterface                      $storeManager
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Neoxero\Revslider\Model\ResourceModel\Layer $resource,
        \Neoxero\Revslider\Model\ResourceModel\Layer\Collection $resourceCollection,
        \Neoxero\Revslider\Model\LayerFactory $layerFactory,
        \Neoxero\Revslider\Model\ResourceModel\Slide\CollectionFactory $slideCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Logger\Monolog $monolog
    ) {
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection
        );
        $this->_layerFactory = $layerFactory;
        $this->_storeManager = $storeManager;
        $this->_slideCollectionFactory = $slideCollectionFactory;

        $this->_monolog = $monolog;
    }

    /**
     * get form field html id prefix.
     *
     * @return string
     */
    public function getFormFieldHtmlIdPrefix()
    {
        return $this->_formFieldHtmlIdPrefix;
    }

    /**
     * get availabe layer.
     *
     * @return []
     */
    public function getAvailableLayers()
    {
        $option[] = [
            'value' => '',
            'label' => __('-------- Please select a slide --------'),
        ];

        $slideCollection = $this->_slideCollectionFactory->create();
        foreach ($slideCollection as $slide) {
            $option[] = [
                'value' => $slide->getId(),
                'label' => $slide->getName(),
            ];
        }

        return $option;
    }

    /**
     * load info multistore.
     *
     * @param mixed  $id
     * @param string $field
     *
     * @return $this
     */
    public function load($id, $field = null)
    {
        parent::load($id, $field);
        return $this;
    }

}
