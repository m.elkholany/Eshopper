<?php

/**
 * Neoxero
 *
 * NOTICE OF LICENSE
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Neoxero
 * @package     Neoxero_Revslider
 */

namespace Neoxero\Revslider\Model;

/**
 * Slide Model
 * @category Neoxero
 * @package  Neoxero_Revslider
 * @module   Revslider
 * @author   Neoxero Developer
 */
class Slide extends \Magento\Framework\Model\AbstractModel
{
    const BASE_MEDIA_PATH = 'neoxero/revslider/slide';

    const SLIDE_TARGET_SELF = 1;
    const SLIDE_TARGET_BLANK = 2;

    /**
     * slider colleciton factory.
     *
     * @var \Neoxero\Revslider\Model\ResourceModel\Slider\CollectionFactory
     */
    protected $_sliderCollectionFactory;

    /**
     * slide factory.
     *
     * @var \Neoxero\Revslider\Model\SlideFactory
     */
    protected $_slideFactory;

    /**
     * [$_formFieldHtmlIdPrefix description].
     *
     * @var string
     */
    protected $_formFieldHtmlIdPrefix = 'page_';

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * logger.
     *
     * @var \Magento\Framework\Logger\Monolog
     */
    protected $_monolog;

    /**
     * [__construct description].
     *
     * @param \Magento\Framework\Model\Context                                $context
     * @param \Magento\Framework\Registry                                     $registry
     * @param \Neoxero\Revslider\Model\ResourceModel\Slide                   $resource
     * @param \Neoxero\Revslider\Model\ResourceModel\Slide\Collection        $resourceCollection
     * @param \Neoxero\Revslider\Model\SlideFactory                     $slideFactory
     * @param \Neoxero\Revslider\Model\ResourceModel\Slider\CollectionFactory $sliderCollectionFactory
     * @param \Magento\Store\Model\StoreManagerInterface                      $storeManager
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Neoxero\Revslider\Model\ResourceModel\Slide $resource,
        \Neoxero\Revslider\Model\ResourceModel\Slide\Collection $resourceCollection,
        \Neoxero\Revslider\Model\SlideFactory $slideFactory,
        \Neoxero\Revslider\Model\ResourceModel\Slider\CollectionFactory $sliderCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Logger\Monolog $monolog
    ) {
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection
        );
        $this->_slideFactory = $slideFactory;
        $this->_storeManager = $storeManager;
        $this->_sliderCollectionFactory = $sliderCollectionFactory;

        $this->_monolog = $monolog;
    }

    /**
     * get form field html id prefix.
     *
     * @return string
     */
    public function getFormFieldHtmlIdPrefix()
    {
        return $this->_formFieldHtmlIdPrefix;
    }

    /**
     * get availabe slide.
     *
     * @return []
     */
    public function getAvailableSlides()
    {
        $option[] = [
            'value' => '',
            'label' => __('-------- Please select a slider --------'),
        ];

        $sliderCollection = $this->_sliderCollectionFactory->create();
        foreach ($sliderCollection as $slider) {
            $option[] = [
                'value' => $slider->getId(),
                'label' => $slider->getTitle(),
            ];
        }

        return $option;
    }

    /**
     * load info multistore.
     *
     * @param mixed  $id
     * @param string $field
     *
     * @return $this
     */
    public function load($id, $field = null)
    {
        parent::load($id, $field);
        return $this;
    }

    /**
     * get target value.
     *
     * @return string
     */
    public function getTargetValue()
    {
        switch ($this->getTarget()) {
            case self::SLIDE_TARGET_SELF:
                return '_self';
            default:
                return '_blank';
        }
    }
}
