<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Neoxero\Homenx\Block;

use Magento\Catalog\Model\Category;
use Magento\Customer\Model\Context;

/**
 * Main contact form block
 */
abstract class Abstracthomenxblock extends \Magento\Framework\View\Element\Template implements \Magento\Framework\DataObject\IdentityInterface {

    /**
     * @var Category
     */
    protected $_categoryInstance;

    /**
     * Current category key
     *
     * @var string
     */
    protected $_currentCategoryKey;

    /**
     * Array of level position counters
     *
     * @var array
     */
    protected $_itemLevelPositions = [];

    /**
     * Catalog category
     *
     * @var \Magento\Catalog\Helper\Category
     */
    protected $_catalogCategory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * Customer session
     *
     * @var \Magento\Framework\App\Http\Context
     */
    protected $httpContext;

    /**
     * Catalog layer
     *
     * @var \Magento\Catalog\Model\Layer
     */
    protected $_catalogLayer;

    /**
     * Product collection factory
     *
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\Indexer\Category\Flat\State
     */
    protected $flatState;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;
    protected $_urlinterface;
    protected $_storeManager;

    /**
     * @var \Magento\Cms\Model\Template\FilterProvider
     */
    protected $_filterProvider;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Catalog\Model\Layer\Resolver $layerResolver
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param \Magento\Catalog\Helper\Category $catalogCategory
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Catalog\Model\Indexer\Category\Flat\State $flatState
     * @param array $data
     */
    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, \Magento\Catalog\Model\CategoryFactory $categoryFactory, \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory, \Magento\Catalog\Model\Layer\Resolver $layerResolver, \Magento\Framework\App\Http\Context $httpContext, \Magento\Catalog\Helper\Category $catalogCategory, \Magento\Framework\Registry $registry, \Magento\Catalog\Model\Indexer\Category\Flat\State $flatState, \Magento\Framework\ObjectManagerInterface $objectManager , \Magento\Cms\Model\Template\FilterProvider $filterProvider, array $data = []
    ) {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_catalogLayer = $layerResolver->get();
        $this->httpContext = $httpContext;
        $this->_catalogCategory = $catalogCategory;
        $this->_registry = $registry;
        $this->flatState = $flatState;
        $this->_categoryInstance = $categoryFactory->create();
        $this->_objectManager = $objectManager;
        $this->_urlinterface = $context -> getUrlBuilder();
        $this->_storeManager = $context -> getStoreManager();
        $this->_filterProvider = $filterProvider;
        parent::__construct($context, $data);
    }

    /**
     * @return void
     */
    protected function _construct() {
        $this->addData(
                [
                    'cache_lifetime' => false,
                    'cache_tags' => [Category::CACHE_TAG, \Magento\Store\Model\Group::CACHE_TAG],
                ]
        );
    }

    /**
     * Get current category
     *
     * @return Category
     */
    public function getCategory() {
        return $this->_registry->registry('current_category');
    }

    /**
     * Get Key pieces for caching block content
     *
     * @return array
     */
    public function getCacheKeyInfo() {
        $shortCacheId = [
            'CATALOG_NAVIGATION',
            $this->_storeManager->getStore()->getId(),
            $this->_design->getDesignTheme()->getId(),
            $this->httpContext->getValue(Context::CONTEXT_GROUP),
            'template' => $this->getTemplate(),
            'name' => $this->getNameInLayout(),
            $this->getCurrentCategoryKey(),
        ];
        $cacheId = $shortCacheId;

        $shortCacheId = array_values($shortCacheId);
        $shortCacheId = implode('|', $shortCacheId);
        $shortCacheId = md5($shortCacheId);

        $cacheId['category_path'] = $this->getCurrentCategoryKey();
        $cacheId['short_cache_id'] = $shortCacheId;

        return $cacheId;
    }

    /**
     * Get current category key
     *
     * @return string
     */
    public function getCurrentCategoryKey() {
        if (!$this->_currentCategoryKey) {
            $category = $this->_registry->registry('current_category');
            if ($category) {
                $this->_currentCategoryKey = $category->getPath();
            } else {
                $this->_currentCategoryKey = $this->_storeManager->getStore()->getRootCategoryId();
            }
        }

        return $this->_currentCategoryKey;
    }

    /**
     * Retrieve child categories of current category
     *
     * @return \Magento\Framework\Data\Tree\Node\Collection
     */
    public function getCurrentChildCategories() {
        $categories = $this->_catalogLayer->getCurrentCategory()->getChildrenCategories();
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection */
        $productCollection = $this->_productCollectionFactory->create();
        $this->_catalogLayer->prepareProductCollection($productCollection);
        $productCollection->addCountToCategories($categories);
        return $categories;
    }

    /**
     * Checkin activity of category
     *
     * @param   \Magento\Framework\DataObject $category
     * @return  bool
     */
    public function isCategoryActive($category) {
        if ($this->getCurrentCategory()) {
            if (is_array($this->getCurrentCategory()->getPathIds()) && in_array($category->getId(), $this->getCurrentCategory()->getPathIds())) {
                return in_array($category->getId(), $this->getCurrentCategory()->getPathIds());
            }
        }
        return false;
    }

    /**
     * Get url for category data
     *
     * @param Category $category
     * @return string
     */
    public function getCategoryUrl($category) {
        if ($category instanceof Category) {
            $url = $category->getUrl();
        } else {
            $url = $this->_categoryInstance->setData($category->getData())->getUrl();
        }

        return $url;
    }

    /**
     * Enter description here...
     *
     * @return Category
     */
    public function getCurrentCategory() {
        return $this->_catalogLayer->getCurrentCategory();
    }

    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities() {
        return [\Magento\Catalog\Model\Category::CACHE_TAG, \Magento\Store\Model\Group::CACHE_TAG];
    }

    /* Homenx Begin */

    public function getModel($model) {
        return $this->_objectManager->create($model);
    }

    public function getStore() {
        return $this->_storeManager->getStore();
    }

    public function getClass($item) {
        //bolck_hook_type
        //$type = $item->getMenuType();
        $type = $item->getBolckHookType();
        $class = $item->getSpecialClass();
        $class.=' ' . $item->getAlignBlock();
        if ($item->getColumns() > 1) {
            $class.= " homenx-block-item homenx-block-fullwidth";
        }
        if ($type == 2) {
            $class.= " static-homenx-block";
            $currentUrl = $this->_urlinterface->getCurrentUrl();
            if ($currentUrl == $item->getUrl()) {
                $class.= " active";
            }

            if ($item->getStaticContent() != '') {
                $class.= ' dropdown';
            }
        } else {

            $categoryId = $item->getCategoryId();
            $category = $this->getModel('Magento\Catalog\Model\Category')->load($categoryId);
            $subCatAccepp = $this->getSubCategoryAccepp($categoryId, $item);

            $class.= " category-homenx-block";

            if (count($subCatAccepp) > 0) {
                $class.= ' dropdown';
            }
            if ($this->isCategoryActive($category)) {
                $store = $this->getStore();
                if ($store->getRootCategoryId() != $category->getId()) {
                    $class.= " active";
                }
            }
        }
        return $class;
    }

    public function getSubCategoryAccepp($categoryId, $item) {
        $subCatExist = explode(',', $item->getSubCategoryIds());

        $category = $this->getModel('Magento\Catalog\Model\Category')->load($categoryId);

        $children = $category->getChildrenCategories();
        $childrenCount = count($children);

        $subCatId = array();
        if ($childrenCount > 0) {
            foreach ($children as $child) {
                if (in_array($child->getId(), $subCatExist)) {
                    $subCatId[] = $child->getId();
                }
            }
        }
        return $subCatId;
    }

    public function getHomenxBlockHtml($item) {
        $type = $item->getBolckHookType();
        if ($type == 2) {
            return $this->getStaticHomenxBlock($item);
        } else {
            return $this->getCategoryHomenxBlock($item);
        }
    }

    public function getCategoryHomenxBlock($item) {
        $blockTitle="";
        $catName="";
        $color="";
        $html="";
        $categoryId = $item->getCategoryId();
        $subCatAccepp = $this->getSubCategoryAccepp($categoryId, $item);
        if ($categoryId) {
            $category = $this->getModel('Magento\Catalog\Model\Category')->load($categoryId);
            $blockTitle = $category->getName();
            $catName = $category->getName();
        }

        // get sub cats
        $arrColumn=[];
        $columnAccepp = count($subCatAccepp);
        if ($columnAccepp > 0) {
            $columns = $item->getColumns();
            if ($item->getLeftContent() != '' && $item->getLeftCol() != 0) {
                $columns = $columns - $item->getLeftCol();
            }

            if ($item->getRightContent() != '' && $item->getRightCol() != 0) {
                $columns = $columns - $item->getRightCol();
            }

            $arrOneElement = array_chunk($subCatAccepp, 1);
            $countCat = count($subCatAccepp);
            $count = 0;
            while ($countCat > 0) {
                for ($i = 0; $i < $columns; $i++) {
                    if (isset($subCatAccepp[$count])) {
                        $arrColumn[$i][] = $subCatAccepp[$count];
                        $count++;
                    }
                }
                $countCat--;
            }

            $newArrColumn = [];
            $newCount = 0;

            for ($i = 0; $i < count($arrColumn); $i++) {
                $newColumn = count($arrColumn[$i]);
                for ($j = 0; $j < $newColumn; $j++) {
                    $newArrColumn[$i][$j] = $subCatAccepp[$newCount];
                    $newCount++;
                }
            }

            $arrColumn = $newArrColumn;
        }

        $color =(string)$item -> getCustomColor();
        $sectionID = uniqid('bareq-antikazone-');
        if( !empty($color)){
          $html.="<style>";
            $html.="#{$sectionID} .bareq-multi-home-block-title{background:#{$color}}"; 
            $html.="#{$sectionID} .bareq-multi-home-block-brands{border-color: #{$color} !important;}";
            $html.="#{$sectionID} .bareq-multi-home-block-content {border-bottom-color: #{$color};border-left-color: #{$color};}";
          $html.="</style>";
        }
        $html.='<section id="'.$sectionID.'" class="bareq-multi-home-block bareq-antikazone'.' '.$sectionID.'"><div class="bareq-multi-home-block-wrap '. $item->getClass().'">';

        // draw block title
        $tmpBlockName = $item->getTitle();
        if ( $tmpBlockName != '') {
             $blockTitle = $tmpBlockName;
        }

        $specialHtml="";// <i class="icon-bareq-dot"></i>
        if( $item->getHtmlLabel() != "" ){
           $specialHtml =  $item->getHtmlLabel();
        }
        if($blockTitle != "" ){
            $html.='<h4 class="bareq-multi-home-block-title">'.$specialHtml.'<span>'.$blockTitle.'</span></h4>';
        }
        // end draw block title

        // draw content
        $html .='<div class="bareq-multi-home-block-content"><div class="row">';

        // - brands col 1
        // $html.='<div class="col-xs-12 col-md-3 col-1"><div class="bareq-multi-home-block-brands">';
        $html.='<div class="col-xs-12 col-md-3 col-1">';
        if ($item->getRightContent() != ''){
            $html.= $this->_filterProvider->getBlockFilter()->filter($item->getRightContent());
        }
        $html .='</div>';
        // $html .='</div></div>';
        // - end brands

        // - cat list and image col 2
        $html .= '<div class="col-xs-12 col-md-5 col-2">';
        // -- image
        if( $item->getBanner() != ""){
            $html .='<div class="bareq-multi-home-block-banner">'. $this->_filterProvider->getBlockFilter()->filter($item->getBanner()).'</div>';
        }

        // -- links

        // \Zend_Debug::dump( $arrColumn , $label = null, $echo = true);

        $html .= '<ul class="bareq-multi-home-block-menu"><li><h4>'.$catName.'</h4>';
        // foreach ($arrColumn as $_arrColumn) {
             // $html.= $this->drawListSub($item, $_arrColumn);
            $html.= $this->drawListSub($item,  $subCatAccepp);
        // }
        $html .='</li></ul>';

        $html .='</div>';
        // - end cat list and image col 2


        // - col 3
        $html .='<div class="col-xs-12 col-md-4 col-3">';
        if ($item->getLeftContent() != ''){
            $html.= $this->_filterProvider->getBlockFilter()->filter($item->getLeftContent());
        }
        $html .='</div>';
        // - end col 3

        $html .='</div></div>';
        // end draw content

        $html.='</div></section>';

        // ---------------

        return $html;
    }

    public function drawListSub($item, $catIds) {
        $html = '';

        //if ($item->getColumns() > 1) {
            //$html.='<div class="col-md-' . $this->getColumnByCol($item->getColumns()) . '"><ul class="sub-homenx-block">';
            $html.="<ul class='sub-menu'>";
       // }

        if (count($catIds) > 0) {
            foreach ($catIds as $categoryId) {
                $category = $this->getModel('Magento\Catalog\Model\Category')->load($categoryId);
                $html.= $this->drawList($category, $item) ;
            }
        }

        //if ($item->getColumns() > 1) {
            $html.='</ul>';
        // }

        return $html;
    }

    public function drawList($category, $item, $level = 1) {
        /* $maxLevel = $item->getMaxLevel();
          if ($maxLevel == '' || $maxLevel == NULL) {
          $maxLevel = Mage::getStoreConfig('homenx/general/max_level');
          }

          if ($maxLevel == 0 || $maxLevel == '' || $maxLevel == NULL) {
          $maxLevel = 100;
          } */
        $maxLevel = 10;

        $children = $this->getSubCategoryAccepp($category->getId(), $item);
        $childrenCount = count($children);

        $htmlLi = '<li';

        if ($childrenCount > 0 && $item->getColumns() == 1) {
            $htmlLi .= ' class="dropdown-submenu"';
        }

        $htmlLi .= '>';

        $html[] = $htmlLi;
        $html[] = '<a href="' . $this->getCategoryUrl($category) . '">';
        if ($item->getColumns() > 1 && $level == 1) {
            $html[] = '<span class="homenx-block-sub-title">';
        }

        $html[] = $category->getName();

        if ($item->getColumns() > 1 && $level == 1) {
            $html[] = '</span>';
        }
        $html[] = '</a>';

        if ($level < $maxLevel) {


            $maxSub = 50;

            $htmlChildren = '';
            if ($childrenCount > 0) {
                $i = 0;
                foreach ($children as $child) {
                    $i++;
                    if ($i <= $maxSub) {
                        $_child = $this->getModel('Magento\Catalog\Model\Category')->load($child);
                        $htmlChildren .= $this->drawList($_child, $item, ($level + 1));
                    }
                }
            }
            if (!empty($htmlChildren)) {
                $html[] = '<span class="toggle-homenx-block visible-xs-block visible-sm-block"><a onclick="toggleEl(this,\'mobile-block-cat-' . $category->getId() . '-' . $item->getParentId() . '\')" href="javascript:void(0)" class=""><em class="fa fa-plus"></em></a></span>';

                $html[] = '<ul id="mobile-block-cat-' . $category->getId() . '-' . $item->getParentId() . '"';
                if ($item->getColumns() > 1) {
                    $html[] = ' class="sub-homenx-block"';
                } else {
                    $html[] = ' class="dropdown-homenx-block"';
                }
                $html[] = '>';
                $html[] = $htmlChildren;
                $html[] = '</ul>';
            }
        }
        $html[] = '</li>';
        $html = implode("\n", $html);
        return $html;
    }

    public function getStaticHomenxBlock($item) {
        $html = '';
        if ($item->getStaticContent() != '') {
            $html.= '';
        }


        // if ($item->getHtmlLabel() != '') {
        //     $html.=$item->getHtmlLabel();
        // }


        // $html.=$item->getTitle();

        // if ($item->getStaticContent() != '') {
        //     $html.= ' <span class="icon-next hidden-xs hidden-sm"><i class="fa fa-caret-down"></i></span>';
        // }


        if ($item->getStaticContent() != '') {
            $staticContent = $this->_filterProvider->getBlockFilter()->filter($item->getStaticContent());
            $html.= '<div class="'.$item->getClass().'">'. $staticContent . '</div>';
        }
        return $html;
    }

    public function getColumnByCol($col) {
        return 12 / $col;
    }

    public function isHomePage() {
        $currentUrl = $this->getUrl('', ['_current' => true]);
        $urlRewrite = $this->getUrl('*/*/*', ['_current' => true, '_use_rewrite' => true]);
        return $currentUrl == $urlRewrite;
    }

}
