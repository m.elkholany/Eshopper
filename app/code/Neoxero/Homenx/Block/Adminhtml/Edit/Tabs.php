<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Neoxero\Homenx\Block\Adminhtml\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('homenx_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Homenx Information'));
    }
	
	protected function _beforeToHtml()
    {
		$this->addTab(
			'main_section',
			[
				'label' => __('General Information'),
				'content' => $this->getLayout()->createBlock('Neoxero\Homenx\Block\Adminhtml\Edit\Tab\Main')->toHtml(),
			]
		);
		

		$this->addTab(
			'category',
			[
				'label' => __('Category'),
				'content' => $this->getLayout()->createBlock('Neoxero\Homenx\Block\Adminhtml\Edit\Tab\Category')->toHtml(),
			]
		);
		
		$this->addTab(
			'topbottom',
			[
				'label' => __('Static Contents'),
				'content' => $this->getLayout()->createBlock('Neoxero\Homenx\Block\Adminhtml\Edit\Tab\Topbottom')->toHtml(),
			]
		);

		$this->addTab(
			'static',
			[
				'label' => __('Static Content'),
				'content' => $this->getLayout()->createBlock('Neoxero\Homenx\Block\Adminhtml\Edit\Tab\Statics')->toHtml(),
			]
		);

		$this->addTab(
			'banner',
			[
				'label' => __('Banner Content'),
				'content' => $this->getLayout()->createBlock('Neoxero\Homenx\Block\Adminhtml\Edit\Tab\Banner')->toHtml(),
			]
		);

		
        return parent::_beforeToHtml();
    }
}
