<?php
/**
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
 
namespace Neoxero\Homenx\Controller\Adminhtml\Helal;

class Show extends \Magento\Framework\App\Action\Action // \Magento\Framework\App\Action\Action
{



	/**
     * Init actions
     *
     * @return $this
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        $this->_view->loadLayout();
        // $this->_setActiveMenu(
        //     'Neoxero_Homenx::homenx_manage'
        // )->_addBreadcrumb(
        //     __('Homenx'),
        //     __('Homenx')
        // );
        return $this;
    }

    /**
     * Check the permission to run it
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Neoxero_Homenx::homenx');
    }




    
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }
    public function execute()
    {
        // echo __METHOD__;
        // exit;

        // \Zend_Debug::dump( "Helal test" , $label ="Test  label of helal", $echo = true);

        $this->_initAction();
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Homenx test Helal'));
        $this->_view->renderLayout();

    }


}
