<?php
/**
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Neoxero\Homenx\Controller\Adminhtml\Homenx;

class Parents extends \Neoxero\Homenx\Controller\Adminhtml\Homenx
{
    public function execute()
    {
        $this->_initAction();
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Homenx Holders'));
        $this->_view->renderLayout();
    }
}
