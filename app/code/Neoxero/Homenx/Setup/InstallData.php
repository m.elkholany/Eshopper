<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Neoxero\Homenx\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
	/**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /**
         * Fill table magev2_protabs
         */
        $data = [
            ['Main Block hook id = 1', 1, NULL, 1],
            ['Hook#2', 2, NULL, 1]
        ];

        $columns = ['title', 'bolck_hook_type', 'custom_class', 'status'];
        $setup->getConnection()->insertArray($setup->getTable('homenx_parent'), $columns, $data);

        // -------------------

        // $dataBlocks = [
        //     ['My label', 2 , 0 ,0 ,0,2,'<hr>Helal test<hr>' ],
        //     ['actual block', 1 , 3 ,0 ,0,2,'','4,5','<p>left</p>','<p>right</p>' ],
        //     ['My label', 1 , 3 ,0 ,0,2,'','6', '<p>left</p>','<p>right</p>' ],
        // ];

        // $columnsBlocks = ['title', 'bolck_hook_type', 'category_id', 'position','store','parent_id','static_content','sub_category_ids','left_content','right_content'];
        // $setup->getConnection()->insertArray($setup->getTable('homenx'), $columnsBlocks, $dataBlocks);

    }
}
