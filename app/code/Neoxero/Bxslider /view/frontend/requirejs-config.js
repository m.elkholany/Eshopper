var config = {
	"map": {
		"*": {
			"Neoxero/bxslider": "Neoxero_Bxslider/js/jquery.bxslider"
		}
	},
	"paths": {            
		"Neoxero/bxslider": "Neoxero_Bxslider/js/jquery.bxslider"
	},
	"shim": {
		"Neoxero_Bxslider/js/vendors/jquery.fitvids": ["jquery"],
		"Neoxero_Bxslider/js/vendors/jquery.easing.1.3": ["jquery"],
		"Neoxero_Bxslider/js/jquery.bxslider": ["jquery"],
	}
};
