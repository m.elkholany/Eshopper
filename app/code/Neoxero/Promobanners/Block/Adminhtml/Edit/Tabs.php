<?php
namespace Neoxero\Promobanners\Block\Adminhtml\Edit;

use Magento\Backend\Block\Widget\Tab\TabInterface;

class Tabs extends \Magento\Backend\Block\Widget\Tabs 
{
    protected function _construct()
    {
		
        parent::_construct();
        $this->setId('checkmodule_promo_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Promo Information'));
    }

    public function isHidden() {
        return FALSE;
    }
//      protected function _prepareLayout()
//        {
//            $this->addTab(
//                'hello',
//                [
//                    'label' => __('Rules'),
//                    'url' => $this->getUrl('adminhtml/promobanners/rules'),
//                    'class' => 'ajax'
//                ]
//            );
//            return parent::_prepareLayout();
//        }

//     protected function _beforeToHtml()
//    {
//        $this->addTab(
//            'news_info',
//            [
//                'label' => __('General'),
//                'title' => __('General'),
//                'content' => $this->getLayout()->createBlock(
//                    'Neoxero\Promobanners\Block\Adminhtml\Edit\Tab\General'
//                )->toHtml(),
//                'active' => true
//            ]
//        );
// 
//        return parent::_beforeToHtml();
//    }
}
