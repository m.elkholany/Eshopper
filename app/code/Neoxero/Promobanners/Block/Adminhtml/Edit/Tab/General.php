<?php
namespace Neoxero\Promobanners\Block\Adminhtml\Edit\Tab;
class General extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = array()
    ) {
        $this->_systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('promobanners_promobanners');
         $form = $this->_formFactory->create();
       

        $form->setHtmlIdPrefix('page_');
       
		 $fieldset = $form->addFieldset('add_promobanners_form', ['legend' => __('Banner Information')]);

        if ($model->getId()) {
            $fieldset->addField('promobanners_id', 'hidden', ['name' => 'promobanners_id']);
        }
        

        $fieldset->addField(
            'title',
            'text',
            [
                'label' => __('Banner Title'),
                'name' => 'title',
                'required' => true
            ]
        );
        
		
		$fieldset->addField(
            'identifier',
            'text',
            [
                'label' => __('Identifier'),
                'name' => 'identifier',
                'required' => true,
				'class' => 'validate-xml-identifier'
            ]
        );

		if($this->getRequest()->getParam('id')){
			$fieldset->addField(
				'filename',
				'file',
				[
					'label' => __('Image'),
					'name' => 'filename',
					'required' => false
				]
			);
		}
		else{
			$fieldset->addField(
				'filename',
				'file',
				[
					'label' => __('Image'),
					'name' => 'filename',
					'required' => true
				]
			);
		}
		
		
		
		$fieldset->addField(
            'content',
            'editor',
            [
                'name' => 'content',
                'label' => __('Content'),
				'style' => 'height:10em',
            ]
        );
		
		$fieldset->addField(
            'button',
            'text',
            [
                'label' => __('Button Text'),
                'name' => 'button',
                'required' => false
            ]
        );
		
		$fieldset->addField(
            'url',
            'text',
            [
                'label' => __('Url'),
                'name' => 'url',
                'required' => false
            ]
        );
		
		$fieldset->addField(
            'text_align',
            'select',
            [
                'label' => __('Text Align'),
                'name' => 'text_align',
                'required' => false,
                'options' => [
					''=> __('Default'),
					'top-left' => __('Top Left'),
					'top-middle' => __('Top Center'),
					'top-right' => __('Top Right'),
					'middle-left' => __('Middle Left'),
					'middle-center' => __('Middle Center'),
					'middle-right' => __('Middle Right'),
					'bottom-left' => __('Bottom Left'),
					'bottom-center' => __('Bottom Center'),
					'bottom-right' => __('Bottom Right')
				]
            ]
        );
		
		$fieldset->addField(
            'effect',
            'select',
            [
                'label' => __('Effect'),
                'name' => 'effect',
                'required' => false,
				'note' => __('Hover on preview image to see'),
                'options' => [
					''=> __('No Effect'),
					'zoom' => __('Effect 1'),
					'border-zoom' => __('Effect 2'),
					'flashed' => __('Effect 3'),
					'zoom-flashed' => __('Effect 4'),
					'shadow-corner' => __('Effect 5'),
					'zoom-shadow' => __('Effect 6'),
					'cup-border' => __('Effect 7'),
					'flashed-zoom' => __('Effect 8'),
					'zoom-out-shadow' => __('Effect 9'),
					'mist' => __('Effect 10'),
					'mist-text' => __('Effect 11'),
				]
            ]
        );
		
		$fieldset->addField(
            'custom_class',
            'text',
            [
                'label' => __('Custom Class'),
                'name' => 'custom_class',
                'required' => false
            ]
        );
		
		$fieldset->addField(
            'status',
            'select',
            [
                'label' => __('Status'),
                'name' => 'status',
                'required' => false,
                'options' => ['1' => __('Enabled'), '0' => __('Disabled')]
            ]
        );

        

        $this->setValues($model->getData());
          $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('General Information');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('General Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
