<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Neoxero\Promobanners\Block\Adminhtml;

/**
 * Sitemap edit form container
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Init container
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml';
        $this->_blockGroup = 'Neoxero_Promobanners';

        parent::_construct();

		if ($this->_isAllowedAction('Neoxero_Promobanners::save')) {
			$this->buttonList->add(
				'saveandcontinue',
				[
					'label' => __('Save and Continue Edit'),
					'class' => 'save',
					'data_attribute' => [
						'mage-init' => ['button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form']],
					]
				],
				-100
			);
		}
//$this->buttonList->add(
//					'create_rule',
//					[
//						'label' => __('Create Rule'),
//						'class' => 'add',
//						'onclick' => 'openRulePopupWindow(\''.$this->getCreateRuleUrl().'\')',
//					],
//					1
//				);
		if ($this->_isAllowedAction('Neoxero_Promobanners::delete')) {
            $this->buttonList->update('delete', 'label', __('Delete'));
        } else {
            $this->buttonList->remove('delete');
        }
//               $this->_formScripts[] = "
//			require(['jquery'], function($){
//				window.openRulePopupWindow = function (url) {
//					var left = ($(document).width()-1000)/2, height= $(document).height();
//					var create_rule_popupwindow = window.open(url, '_blank','width=1000,resizable=1,scrollbars=1,toolbar=1,'+'left='+left+',height='+height);
//					var windowFocusHandle = function(){
//						if (create_rule_popupwindow.closed) {
//							if (typeof layerGridJsObject !== 'undefined' && create_rule_popupwindow.layer_id) {
//								layerGridJsObject.reloadParams['layer[]'].push(create_rule_popupwindow.layer_id + '');
//								$(edit_form.slide_layer).val($(edit_form.slide_layer).val() + '&' + create_rule_popupwindow.layer_id + '=' + Base64.encode('order_layer_slide=0'));
//				       			layerGridJsObject.setPage(create_rule_popupwindow.layer_id);
//				       		}
//				       		$(window).off('focus',windowFocusHandle);
//						} else {
//							$(create_rule_popupwindow).trigger('focus');
//							create_rule_popupwindow.alert('".__('You have to save layer and close this window!')."');
//						}
//					}
//					$(window).focus(windowFocusHandle);
//				}
//			});
//		";
    }

    /**
     * Get edit form container header text
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('promobanners_promobanners')->getId()) {
            return __('Edit %1', $this->_coreRegistry->registry('promobanners_promobanners')->getTitle());
        } else {
            return __('New Banner');
        }
    }
	
	/**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
    
    	/**
     * get create slide url.
     *
     * @return string
     */
    public function getCreateRuleUrl()
    {
        return $this->getUrl('catalog_rule/promo_catalog/new');
    }
}
