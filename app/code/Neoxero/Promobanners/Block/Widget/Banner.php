<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Neoxero\Promobanners\Block\Widget;

/**
 * Cms Static Block Widget
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Banner extends \Magento\Catalog\Block\Product\AbstractProduct implements \Magento\Widget\Block\BlockInterface
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;
     protected $_productRepository;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Cms\Model\Template\FilterProvider $filterProvider
     * @param \Magento\Cms\Model\BlockFactory $blockFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
		\Magento\Framework\ObjectManagerInterface $objectManager,
            \Magento\Catalog\Model\ProductRepository $productRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_productRepository = $productRepository;
		$this->_objectManager = $objectManager;
    }

    public function getModel(){
		return $this->_objectManager->create('Neoxero\Promobanners\Model\Promobanners');
	}
        
         public function getProductById($id)
    {
        return $this->_productRepository->getById($id);
    }
	
	public function getBannerById($id){
		if (!is_numeric($id)) {
            $banner = $this->getModel()->getCollection()->addFieldToFilter('identifier', $id)->getFirstItem();
			if($banner->getId()){
				return $banner;
			}else{
				return;
			}
        }else{
			$banner = $this->getModel()->load($id);
			return $banner;
		}
	}
	
	public function getBannerImageUrl($banner){
		$bannerUrl = $this->_urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) . 'promobanners/'.$banner->getFilename();
		return $bannerUrl;
	}
	
	public function getCustomClass($banner){
		$class = '';
		if($banner->getCustomClass()!=''){
			$class .= ' '.$banner->getCustomClass();
		}
		if($banner->getEffect()!=''){
			$class .= ' '.$banner->getEffect();
		}
		return $class;
	}
}
